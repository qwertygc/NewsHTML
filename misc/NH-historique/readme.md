-   Version 4 (28/11/2015) : On enlève NHTags. Changement de la fonction
    du human reading de la taille des fichiers.
-   [Version 3.99](archives/v399.zip) (08/11/2015) : Change l’URL pour
    télécharger PubSubHubBub.
-   Version 3.98 (08/11/2015) : Correction bug bise à jour bibliothèque.
-   Version 3.97 (15/09/2015) : Passage à la licence Cecill.
-   Version 3.96 (15/09/2015) : Correction quelques problèmes avec
    le courriel.
-   Version 3.95 (14/09/2015) : Correction quelques problèmes mineurs.
-   Version 3.94 (12/09/2015) : Fixe bug mail.
-   Version 3.93 (12/08/2015) : Recherche avec guillement fonctionant.
-   Version 3.92 (30/07/2015) : Amélioration pour la gestion des liens
    (import/export, flux rss spécifique, redirige vers la page si le
    lien existe déjà).
-   Version 3.91 (25/07/2015) : Ajout de PubHubSubBub, de h-entry et
    téléchargement des bibliothèques.
-   Version 3.90 (20/06/2015) : Autofocus sur le textarea. correction
    bug faisant planter index.php. Mise en place d'un proto-shaarli.
-   Version 3.89 (14/06/2015) : Un administrateur peut poster au nom
    d’un rédacteur. Possibilité étendue dans la personnalisation des
    URLS
-   Version 3.88 (30/04/2015) : Tri par date+ correction bug
    par brouillon.
-   Version 3.87 (29/04/2015) : On peut modérer à posteori (et
    pas supprimer).
-   Version 3.86 (16/04/2015) : Correction bug dans la recherche.
-   Version 3.85 (16/04/2015) : Suppression du choix de la pagination,
    mais création d'une template adéquate (pagination.php).
-   Version 3.84 (15/04/2015) : Choix du type de pagination.
-   Version 3.83 (15/04/2015) : Modification tradition français
-   Version 3.82 (17/02/2015) : Correctipn bug tag
-   Version 3.81 (14/02/2015) : Correction bugs diverses dont le lien
    invalible dans le courriel
-   Version 3.80 (14/12/2014) : Correction bug demonstration + bug flux
    rss vide
-   Version 3.79 (30/11/2014) : Création d'une option de démonstration +
    maj automatique des bibliothèques. Pensez à rajouter define('DEMO',
    false); dans config.php sous peine de tout bloquer !
-   Version 3.78 (29/11/2014) : - Correction bug redirection
    installation, correction bug création de base mysql, correction bug
    concernant le non listage des articles privés
-   Version 3.77 (10/11/2014) : Ajout du enable\_cache dans le fichier
    langue
-   Version 3.76 (09/11/2014) : Correction d'une faille qui permet à un
    simple auteur de modifier les données d'un autre auteur. Création
    d'une fonction check\_cache() qui permet d'alléger le code.
    Simplification de l'installation
-   Version 3.75 (09/11/2014) : On n'affiche plus les articles privés ni
    dans la liste des articles, ni dans la recherche ni dans
    la recherche.
-   Version 3.74 (03/10/2014) : Amélioration ATOM + n'affiche pas les
    notes de pied de page quand il n'y en a pas.
-   Version 3.73 (02/10/2014) : Modification de form.class.php. On crée
    atom.class.php, et on ne garde plus les flux RSS. Rajout du suivi de
    commentaires par ATOM.
-   Version 3.72 (20/09/2014) : Correction bug du système du système de
    cache quand un article est sans commentaire.
-   Version 3.71 (15/09/2014) : Ajout d’un système de pagination
    dans l’administration.
-   Version 3.70 (13/09/2014) : Quand on modifie TAG\_SEPARATOR, cela
    édite les articles pour que cela soit prise en compte.
-   Version 3.69 (09/09/2014) : Correction bug cache. (remettant en
    cause 3.66)
-   Version 3.68 (01/09/2014) : On base l'affichage des dates sur
    le setlocal.
-   Version 3.67 (30/08/2014) : Lien vers l'article dans l'admin.
-   Version 3.66 (25/08/2014) : Mise à jour lib parsedown + Mise en
    cache totale des articles.
-   Version 3.65 (21/08/2014) : Création d'un système de sauvegarde.
-   Version 3.64 (18/08/2014) : Affiche une erreur quand une entrée
    linguistique n'existe pas.
-   Version 3.63 (16/08/2014) : Choix du langage de mise en forme entre
    HTML, Markdown et NHtags. Correction du bug du cache du timezone
    dans l'administration.
-   Version 3.62 (11/08/2014) : Réécriture de l'installateur.
-   Version 3.61 (10/07/2014) : Correction du bug du HTML dans la balise
    code qui était lu dans le navigateur. Correction bug administration
    qui voulait pas se déconnecter à cause du cookie. On peut suivre par
    RSS à la fois un auteur et un tag particulier.
-   Version 3.60 (05/07/2014) : Amélioration interface téléversement +
    liens cliquable mail de notification de nouveaux commentaires.
-   Version 3.59 (02/07/2014) : Correction bug footnote (3)
    (merci last-geek).
-   Version 3.58 (01/07/2014) : Correction bug footnote (2).
-   Version 3.57 (01/07/2014) : Correction bug footnote.
-   Version 3.56 (30/06/2014) : Serialization cookies + création cookie
    autoconnexion administration.
-   Version 3.55 (26/06/2014) : Correction bug feed.php.
-   Version 3.54 (21/06/2014) : Possibilité utiliser un masque pour le
    format d'URL (necessite de faire à la main l'url rewrite).
-   Version 3.53 (15/06/2014) : Possibilité d'avoir des articles
    protégés par des mots de passe et les articles en brouillons peuvent
    être vu par des administrateur en tant qu'articles normales.
-   [Version 3.52](archives/v352.zip) (01/06/2014) : Correction bug dans
    la fonction send\_mail
-   Version 3.51 (31/05/2014) : Pour un simple auteur, on affiche
    uniquement les commentaires de ses articles. Quand on supprime un
    auteur, on supprime aussi les commentaires associés à ses articles.
-   Version 3.50 (24/05/2014) : Il y a plus de formulaire quand les
    commentaires sont désactivés.
-   Version 3.49 (23/05/2014) : Ajout d'un système de pallier pour
    l'estimation du temps.
-   Version 3.48 (17/05/2014) : Correction bug encodage.
-   Version 3.47 (01/05/2014) : Pagination moins buggée.
-   Version 3.46 (01/05/2014) : Correction bug avec le cache.
-   Version 3.45 (28/04/2014) : Un auteur peut modifier son profil +
    réorganisation de la configuration.
-   Version 3.44 (28/04/2014) : Ajout d'une pagination efficace.
-   Version 3.43 (27/04/2014) : Estime la durée de lecture d'un article.
-   Version 3.42 (20/04/2014) : Possibilité de désactiver le cache.
-   Version 3.41 (14/04/2014) : Correction bug installation (erreur
    sql author). Correction bug connexion administration (erreur sql
    table author). Correction bug suppression cache.
-   Version 3.40 (06/04/2014) : amélioration du parsage avec
    les paragraphes.
-   Version 3.39 (29/03/2014) : Note de pied de page dans le flux et
    amélioration du parsage avec les paragraphes.
-   Version 3.38 (23/03/2014) : Ajout des flux Atom
-   Version 3.37 (08/03/2014) : Système de note de pied de page
    \\footnote{}
-   Version 3.36 (07/03/2014) : J'ai rajouté des trucs pour
    la typographie.
-   Version 3.35 (04/03/2014) : Création du multi-utilisateur et ajout
    de deux trois trucs dans la syntaxe.
-   Version 3.34 (16/02/2014) : Correction bug cache article.
-   Version 3.33 (26/01/2014) : Correction bug parsage italique.
-   Version 3.32 (25/01/2014) : On supprime le parsage pour la vidéo
    et l'audio.
-   Version 3.31 (21/01/2014) : Amélioration ergonomie avec la césure du
    texte par le nombre de mots et non par le nombre de caractères.
-   Version 3.30 (10/12/2013) : Allégement du code avec la
    fonction sql().
-   Version 3.29 (10/12/2013) : Amélioration du système d'erreur +
    correction de quelqu'uns.
-   Version 3.28 (04/12/2013) : Mise en cache de la liste des fuseaux
    horaires, on divise par 2 la consommation de ram.
-   Version 3.27 (04/12/2013) : Correction du bug avec l'italique
-   Version 3.26 (31/12/2013) : Les formulaires sont générés en PHP,
    Correction le bug dans l'adresse du dossier d'upload dans l'admin,
    création d'un captcha
-   Version 3.25 (30/12/2013) : Possibilité du choix de caractères pour
    séparer les tags.
-   Version 3.24 (29/12/2013) : inc/config.php, uploads/ et data.sqlite
    sont maintenant dans le dossier data/.
-   Version 3.23 (28/12/2013) : L’envoie des mails se fait en HTML et
    en plain/text.
-   Version 3.22 (27/12/2013) : Enfin une pagination qui marche +
    option, si existe dans les templates d'une toolbar.
-   Version 3.21 (26/12/2013) : Possibilité d'invalider un commentaire
    dans la moderation a priori
-   Version 3.20 (24/12/2013) : ☃ Christmas Release : Durée de vie du
    cache RSS et page d'une heure 'pour les articles programmés)
-   Version 3.19 (23/12/2013) : Ajout de quelques nouveaux codes pour le
    langage de balisage
-   Version 3.18 (13/12/2013) : Quelques optimisations de performances
-   Version 3.17 (13/12/2013) : J'enlève la class des formulaires mais
    j'allège certains trucs dans l'admin.
-   Version 3.16 (13/12/2013) : Le formulaire des commentaires n'est
    plus prise en compte dans le cache : le cookie des autres
    est affiché.
-   Version 3.15 (13/12/2013) : Création d'une class pour
    les formulaires.
-   Version 3.14 (13/12/2013) : On vide le cache quand on
    supprime/valide un commentaire.
-   Version 3.13 (12/12/2013) : J'enlève le setlocale, ça marche pas.
-   Version 3.12 (11/12/2013) : Liste déroulante qui marche à peu pret
    (surtout pour la france).
-   Version 3.11 (11/12/2013) : Input pour le setlocale. Vu que c'est
    une option avancée, et que ça change selon les OS.
-   Version 3.10 (11/12/2013) : Choix de la langue par défaut avec
    setlocale
-   Version 3.09 (10/12/2013) : Création du SETLOCALE
-   Version 3.08 (07/12/2013) : Correction bug du debug mod dans
    l'administration
-   Version 3.07 (07/12/2013) : Compression avant d'envoyer au client.
    Correction du bug du cache sur les tags du RSS
-   Version 3.06 (04/12/2013) : Ajout d'un système de cache.
-   Version 3.05 (27/11/2013) : Exposant+indice en markdown.
-   Version 3.04 (26/11/2013) : Si un design commence par un point, il
    n'est pas affiché.
-   Version 3.03 (24/11/2013) : Possibilité d'activer le debug depuis
    l'administration + choix des formats des dates.
-   Version 3.02 (23/11/2013) : Amélioration debug (ajout info
    mémoire consommée). Upload multifichiers. Correction bugs affichage
    du nombre de commentaires quand ils sont désactivés.
-   Version 3.01 (17/11/2013) : Réorganisation totale des dossiers.
    Simplification du fichier search.php. Le système des templates a été
    revu, on utilise maintenant le système natif de PHP. On renomme img/
    en upload/ et on met à l'intérieur upload.txt. On supprime le double
    mot de passe à l'installation.
-   [Version 3](archives/v3.zip) (17/11/2013) : Je considère que la
    version est assez stable pour passer en v3
-   Version 2.99 (16/11/2013) : Plus de parsage dans la balise code
    (merci fab@c++)
-   Version 2.98 (02/11/2013) : Quand on désactive les commentaires,
    juste le formulaire est enlevé
-   Version 2.97 (01/11/2013) : Correction bug pagination (plus de page
    vide dû au brouillon). Ajout d'un die() pour la redirection.
    Optimisation de quelques requêtes SQL
-   Version 2.96 (31/10/2013) : Dans le markdown, on rajoute les listes
    non ordonnées et ordonnées
-   Version 2.95 (29/10/2013) : Correction de quelques détails pour
    l’internationalisation + correction sémantique (on enlève les br
    dans certains cas lors de la rédaction)
-   Version 2.94 (27/10/2013) : Modification de la fonction StripAccent.
    Mail de confirmation : utf-8 + un mail d’expéditeur
-   Version 2.93 (21/10/2013) : Un email unique envoyé par article et
    par commentateur (correction bug).
-   Version 2.91 (14/10/2013) : Un email unique envoyé par article et
    par commentateur.
-   Version 2.90 (05/10/2013) : Quand on supprime un article, on
    supprime les commentaires associés
-   Version 2.89 (05/10/2013) : Correction d'un bug quand on modifie le
    mot de passe. C'est plus un mot de passe vide !
-   Version 2.88 (05/10/2013) : Ooops... Bug avec les tags. Ont les voit
    maintenant sur la liste d'article.
-   Version 2.87 (05/10/2013) : Installation full english (désolé) +
    vérification du mot de passe
-   Version 2.86 (22/09/2013) : Correction coquille ancre
    des commentaires. Amélioration du code avec les
    opérations ternaires. Les avatars sont dans le dossier cache/avatar.
    Correction bug dans le ban ip (le fichier ne se créait pas)
-   Version 2.85 (18/09/2013) : Ajout de l'ancre dans l'administration
    pour les commentaires
-   Version 2.84 (07/09/2013) : Pour l'installation, MySQL est en PDO +
    un système anti-brute force pour l'administration (fonctions
    de sebsauvage.net)
-   Version 2.83 (01/09/2013) : Oups, toutes les infos de config.php
    n'était pas prise en compte avec le maj.php. Mea Culpa
-   Version 2.82 (28/08/2013) : Correction d'un bug avec maj.php.
-   Version 2.81 (28/08/2013) : Le timezone est sélectionné par défaut
    dans l'installation et l'administration. Ajout d'un système de
    hashage des MDP.
-   Version 2.80 (15/08/2013) : Ajout d'un timezone dans
    les configurations. Création d'une fonction redirect pour faire les
    redirections
-   Version 2.79 (08/08/2013) : Simplification du système
    d'authenfication de l'administration + déplacement des logs dans le
    dossier cache.
-   Version 2.78 (07/08/2013) : Possibilité de la validation des
    commentaires a priori. Utilisation de scandir au lieu de l'ersatz
    de PHP4. Factorisation de certains codes avec des fonctions.
-   [Version 2.77](archives/v277.zip) (03/08/2013) :Remplacement de
    htmlentities par htmlspecialchars pour l'utilisation de l'utf-8.
-   Version 2.76 (02/08/2013) :Ajout d'un système de log en cas d'échec
    de connexion.
-   Version 2.75 (24/07/2013) :Protection contre les failles CRSF dans
    l'administration (suppression). Modification du ReadMe, pour le
    rendre plus complet, et agréable à lire.
-   Version 2.74 (23/07/2013) :Simplification code recherche. Tri du
    flux RSS par les tags
-   Version 2.73 (06/07/2013) :Dans la template, prise en compte de la
    langue choisi par le fichier langue (lang="fr")
-   Version 2.72 (08/06/2013) :Correction d'un bug qui mettait une date
    de novembre 1999 quand on laissait le champs date vide
-   Version 2.71 (02/06/2013) :Le flux RSS accepte maintenant le BBCode
-   Version 2.70 (20/05/2013) :Amélioration du parsage (plus de liens
    qui se mettent en italique à l'intérieur même de la structure).
    Ajout de l'audio et vidéo en HTML5 dans le Markdown. Amélioration
    détection du type de fichier dans le téléversement. Oublie d'un
    PREFIXE dans index.php
-   Version 2.69 (07/05/2013) : Quand on téléverse un fichier audio ou
    vidéo, la balise d'insertion HTML5 vidéo ou audio est inclue, à
    l'instar de l'image.
-   Version 2.68 (04/05/2013) : Ajout d'un prefixe manquant dans
    index.php
-   Version 2.67 (01/04/2013) : Correction d'un bug avec les cookies (au
    poisson, le cookie \^\^)
-   Version 2.66 (25/03/2013) : Correction d'un bug quand on éditait un
    article (mise dans le bonne ordre pour la requête préparée)
-   Version 2.65 (17/03/2013) : Allégement du code avec des requêtes
    préparés en ?
-   Version 2.64 (16/03/2013) : Modification pour la notif par mail : La
    notif globale marche. Plus de cookie pour retenir les notifs par
    mail, sinon on est notifié pleins de fois.
-   Version 2.63 (02/03/2013) : Correction de bugs dans l'email de
    notification : Le mail était envoyé même si c'était vous qui
    écrivait le message, le pseudo et l'id du commentaire associé est
    corrigé (avant, elle affichait celle du posteur qui voulait suivre)
    .
-   Version 2.62 (01/03/2013) : Création d'un langage de balisage léger.
    Correction d'un bug (br qui s'ajoutait) avec les listes
    numériques (ol). Rajout d'un datetime dans les templates {datetime}.
-   Version 2.61 (28/02/2013) : Dans la configuration de
    l'administration, mise en place de fichier langues pour certains
    termes au lieu que ça soit codé en dur. Amélioration du design. On
    vérifie qu'on poste pas un commentaire vide. Possibilité de couper
    un article (et de choisir le nombre de caractères). Possibilité de
    modifier dans l'administration le nombre d'articles affichés dans
    l'accueil (même valeur que dans les flux RSS).
-   Version 2.60 (25/02/2013) : Création d'un mode de débogue (temps
    d'exécution + nombre de requêtes + error\_reporting),
    restructuration de l'arborescence design, nouveau design,
    déconnexion de l'administration est au même plan que les autres
    onglets, correction d'un bug quand on modifie le titre, le slashes
    n'est pas enlevé.
-   Version 2.59 (16/02/2013) : Correction d'une faille d'injection SQL
    dans la recherche, recherche dans le contenu et le titre et un
    message s'affiche quand il n'y a pas de résultats. Correction d'un
    bug XSS dans les cookies. Oublie d'une parenthèse dans le maj.php.
    (Merci Hoy) Correction, lors de l'upload, d'une variable
    non déclaré.
-   Version 2.58 (14/02/2013) : ♥ St Valentin Release II : Requêtes
    préparées pour le mysql.
-   Version 2.57 (14/02/2013) : ♥ St Valentin Release : Vérifions
    l'article est présent dans la base de donnée sinon, ça affiche une
    page d'erreur (correction d'un bug), correction d'un bug permettant
    de voir le titre des articles programmés. Mise dans le config le
    nombre d'articles qu'on voulait.
-   Version 2.56 (03/02/2013) : Un bug dans l'installation. Ca envoyait
    pas le moteur de base de donnée lors de l'installation (oublie de
    l'attribut name)
-   Version 2.55 (19/01/2013) : Affichage ou non du menu de
    configuration de Mysql selon le choix dans l'installation.
-   Version 2.54 (11/01/2013) : Dans le fichier de configuration, ce
    sont maintenant des constantes.
-   Version 2.53 (04/01/2013) : stripslashes dans le titre du title +
    tout les fichiers en utf-8
-   Version 2.52 (24/12/2012) : Correction d'un bug empêchant la
    suppression d'un article.
-   Version 2.51 (23/12/2012) : Ajoute un petit gestionnaire lors de
    l'upload (listing des fichiers upload + informations sur eux).
    Possibilité d'uploader des fichiers et plus que des images.
-   Version 2.50 (19/12/2012) : Pour SQLITE : requêtes préparés (et donc
    corrections de bugs éventuels) + correction bug title (le
    sous titre).
-   Version 2.49 (16/12/2012) : Correction d'un bug avec ' et " lors de
    la création ou la modification d'article avec SQLITE.
-   Version 2.48 (03/12/2012) : annule nl2br dans pre, ul et table
-   Version 2.47 (01/12/2012) : Mise du titre de l'article dans
    le title. Correction d'un bug dans la recherche de tags (= au lieu
    de == :/). Possibilité d'un flux RSS par tag (rss.php?tag=montag)
-   Version 2.46 (25/11/2012) : Correction d'un bug dans l'onglets de
    l'administration, affichait pas le terme mais le nom de l'onglet
    (de l'URL).
-   Version 2.45 (06/11/2012) :
    -   J'avais oublié de définir la variable \$brouillon quand on
        créait un nouvel article
    -   Suppression de la variable langue inutile onlyimage dans
        l'upload
    -   Problème quand on vide les tampons (ob\_end\_flush). A la place,
        mettre while (@ob\_end\_flush()). Merci la doc !
    -   Correction d'un bug avec les cookies. Quand on demandais une
        confirmation par email, ça mettait notif en guise de site !
    -   Correction d'un bug quand on supprime un commentaire (oublier de
        remplacer un terme après avoir copié-collé le code)
    -   Correction d'un bug avec les onglets de l'administration (si on
        était dans la page qui listait les articles, les autres onglets
        ne savait pas s'il devait mettre un lien ou non).
    -   Correction du bug pour le cache des
        gravatars (http://wiki.suumitsu.eu/doku.php?id=php:cache\_gravatar)
-   Version 2.44 (02/11/2012) :
    -   Proposition du MySQL/SQLITE lors de l'installation.
    -   Le formulaire des commentaires est après les commentaires
        (\$comments = \$comments.''.\$output; dans index.php. Pour ceux
        qui veulent remettrent comme avant, il suffit d'inverser les
        deux variables)
    -   Redirection depuis toutes les pages vers l'installation si rien
        n'est installé (on déplace install() dans system.php)
-   Version 2.43 (22/08/2012) :
    -   Dans l'administration, on indique sur la page que l'on est par
        un onglet actif (merci fab@c++ pour ton aide pour la gestion de
        certains bugs).
    -   Mise d'un article d'exemple et d'un commentaire d'exemple.
-   Version 2.42 (18/08/2012) :
    -   Correction d'un bug avec les ' et " lors de l'installation
    -   Correction d'un bug dans la recherche qui affiche une page
        blanche (oublie d'un = au lieu de == dans la vérification
        d'un brouillon)
    -   Dans la MAJ, si la colonne brouillon n'existe pas on la crée
    -   Correction d'une faille XSS dans la modification d'un article
    -   On indique que l'upload n'est que pour les images
    -   Ajout un favicon sur le site (je suis pas très créatif :-° Si
        vous avez mieux, vous pouvez me proposer)
    -   On fusionne les cookies en un. Il est nommée cookie\_newshtml.
-   Version 2.41 (17/08/2012) : Ajout d'une possibilité d'une mise
    en brouillon.
-   Version 2.40 (15/08/2012) : Les commentaires vides ne peuvent plus
    être postés.
-   Version 2.39 (09/08/2012) : Oublie d'un \$prefixe dans index.php.
-   Version 2.38 (18/07/2012) : Lorsque qu'on liste les news dans la
    pagination, on exclue les articles du futur (sinon, si on avait
    programmé 7 news, il n'affichait que 3).
-   Version 2.37 (17/07/2012) : Suppression du listage des images.
-   Version 2.36 (04/07/2012) : Séparation du \$termes\['newversion'\]
    en deux morceaux : \$termes\['newversion'\] et
    \$termes\['download'\] (pour la traduction, plus simple, car plus de
    de HTML)
-   Version 2.35 (11/06/2012) : Affinage du moteur de recherche en
    autorisant plusieurs mots clés.
-   Version 2.34 (11/06/2012) : Ajout d'un Regex en HTML5 pour le format
    de date dans l'éditeur d'article.
-   Version 2.33 (08/06/2012) : Trie par ordre ant-chronologique dans
    le RSS.
-   Version 2.32 (08/05/2012) : Dans l'administration, dans la
    configuration, l'input du mot de passe est passé en type="password".
-   Version 2.31 (06/05/2012) : Lien du titre enlevé si on est sur un
    article
-   Version 2.3 (01/05/2012) :
    -   Ajout d'une organisation sous forme de template (tpl/)
        (Merci alex)
    -   Cache des gravatars
-   Version 2.28 (29/04/2012) :
    -   Ajout du méta rss dans le header
    -   Migration de header.php et footer.php dans tpl/
-   Version 2.26 (28/04/2012) : Correction d'une coquille empéchant de
    voir les articles si les commentaires ne sont pas activé
-   Version 2.25 (27/04/2012) :
    -   Triage des articles par date et non plus par id
    -   Correction du maj.php qui veut pas se supprimer (en localhost en
        tout cas) (merci alex, une coquille dans une requête SQL)
    -   encodage de la base de donnée des commentaires
    -   Empêcher de chercher une news qui n'est pas encore paru.
    -   indentation d'une partie du code
    -   modification mineure du code dans index.php
    -   Je commente un peu le code
-   Version 2.2 (23/04/2012) :
    -   Faire un préfixe pour le SQL (merci Alex)
    -   Faire un installateur SQL pour les mods (prendre en compte
        le préfixe)
    -   Traduire "préfixe" dans les fichiers langues
-   Version 2.10 (18/04/2012) :
    -   Editer son design en ligne (mods ?)
    -   Notification par email de nouveaux commentaires (tester
        l'envoie d'email)
    -   Enlever le .php dans l'affichage des plugins
    -   Lors de l'installation, indiquer l'adresse complète et pas
        seulement de domaine
    -   Dans l'administration, dans la configuration, garder les valeurs
        des listes déroulantes + des cases cochées. (Merci Alex)
    -   Possibilité de planifier des articles
    -   Enlever les news.php?id= du code (notamment dans la recherche)
    -   Ancre aux commentaires
    -   Correction de l'outil de MAJ
-   [Version 2.0](archives/v2.zip) (10/04/2012) : maj.php pointe vers
    l'admin et non l'index. Dans les paramètres, on garde en mémoire si
    les commentaires sont activés ou non.
-   Version 1.99 (10/04/2012) : Après la modification les paramètres,
    redirection vers la page de modification
-   Version 1.98 (10/04/2012) : Lors de l'installation,le lien "retour
    vers l'administration" pointe vers admin.php et non ../admin.php
-   Version 1.97 (10/04/2012) : Lors de l'installation,le lien de
    "retour vers l'index" est renommé en "retour vers l'administration"
-   Version 1.96 (03/04/2012) :Possibilité de supprimer une image
    (page upload).
-   Version 1.95 (30/03/2012) : Retour à l'ancienne adresse.
-   Version 1.94 (23/03/2012) : Correction bug dans la modification
    du config.php.
-   Version 1.93 (23/03/2012) : Possibilité de modifier config.php
    depuis l'administration.
-   Version 1.92 (24/02/2012) : description mis sur le site en général.
-   Version 1.91 (24/02/2012) : Choix du design lors de l'installation.
-   Version 1.90 (15/02/2012) : Système d'activation/désactivation de
    commentaires pour des news spécifiquement.
-   [Version 1.89](archives/v189.zip) (11/02/2012) : Ajout
    d'un usleep(10000) pour lutter contre les attaques brutes-forces
-   Version 1.88 (19/01/2012) : Fusion de upload et de uploadpage dans
    l'admin (héritage du Jquery).
-   Version 1.87 (31/12/2011) : Ajout des addslashes pour protéger des
    failles SQL.
-   Version 1.86 (30/12/2011) : Création du dossier img/ lors
    de l'installation.
-   Version 1.85 (30/12/2011) : Ajout d'un header xml dans le flux RSS.
-   Version 1.84 (28/12/2011) : NewsHTML à 1an ! Le nom du site se met
    dans le title et sur le site et plus seulement dans le RSS.
-   Version 1.83 (20/12/2011) : Traduction de l'installation en anglais.
-   Version 1.82 (20/12/2011) : Une grosse partie des commentaires du
    code ont été enlevé.
-   Version 1.81 (20/12/2011) : Fusion du fichier index.php et
    de news.php.
-   Version 1.80 (11/12/2011) : Création de upload.txt et de base.sql
    lors de l'installation. Ajout d'un système de lien pour les tags.
-   Version 1.79 (03/12/2011) : Ajout d'un système de cookies.
-   Version 1.78 (03/12/2011) : Ajout d'un système de tag.
-   Version 1.77 (30/11/2011) : L'upload est à nouveau possible et
    correction d'un bug dans les commentaires.
-   [Version 1.76](archives/v176.zip) (28/11/2011) : L'installation
    n'est plus install.php?action mais install.php.
-   Version 1.75 (23/11/2011) : Je change de serveur, nouvelle url.
-   Version 1.74 (22/11/2011) : Je corrige une erreur gramatical.
    Maintenant, c'est 1 commentaire et 2 commentaires. L'accord est mis.
-   Version 1.73 (21/11/2011) : Je crée un fichier install.php pour
    unifier le dossier en un fichier. Le fichier, de 16Ko passe à 15ko.
-   Version 1.72 (19/11/2011) : Je met l'administration au root.
-   Version 1.71 (12/11/2011) : Fusion de fichiers de l'administration.
-   Version 1.70 (11/11/2011) : Correction d'un bug dans l'encodage.
-   Version 1.69 (11/11/2011) : J'enlève le JS superflue.
-   Version 1.68 (11/11/2011) : Dans l'administration, correction du bug
    pour accéder aux différentes rubriques.
-   Version 1.67 (31/10/2011) : Correction d'un bug dans le design.
-   Version 1.66 (31/10/2011) : Correction d'un bug dans la création
    de news.
-   Version 1.65 (30/10/2011) : Je Supprime Jquery. De 347Ko, je passe
    à 37,5Ko. L'upload se fait dans un popup.
-   Version 1.64 (30/10/2011) : Quand le login/mot de passe est faux
    dans l'administration, redirection vers la boite de login, et on
    reste plus bloqué sur erreur.
-   Version 1.63 (30/10/2011) : Ajouter du nombre de commentaires
    par news.
-   Version 1.62 (30/10/2011) : En enlevant les commentaires superflues
    du code source, de 351Ko, je passe à 347Ko.
-   Version 1.61 (30/10/2011) : On remplace require\_once par includes.
-   Version 1.60 (30/10/2011) : Amélioration du design des commentaires.
-   Version 1.59 (30/10/2011) : Amélioration de la sémantique et
    alignement du formulaire des commentaires.
-   Version 1.57 (29/10/2011) : Mise en place d'un nouveau design
    par défaut.
-   Version 1.56 (29/10/2011) : Liste déroulante proposant les langues
    lors de l'installation.
-   Version 1.55 (28/10/2011) : Mise par défaut des commentaires.
-   Version 1.54 (28/10/2011) : Redirection vers l'installation si
    pas installé.
-   Version 1.53 (28/10/2011) : Transformation en PDO.
-   Version 1.52 (13/09/2011) : Lors de la pagination, le lien est
    enlever sur la page actif.
-   Version 1.51 (09/09/2011) : Correction d'un bug dans
    l'identification de l'administration. En effet, les mots de passes
    n'était pas vérifié.
-   Version 1.50 (30/07/2011) : Mauvaise variable dans l'url absolu.
    Désolé \^ !
-   Version 1.49 (29/07/2011) : Correction d'un bug dans la vérification
    des identitifants de l'administration.
-   [Version 1.48](archives/v148.zip) (26/07/2011) : optimisation du
    parsage de lien.
-   Version 1.47 (25/07/2011) : Correction d'un bug dans la vérification
    d'une nouvelle version.
-   Version 1.46 (22/07/2011) : Url de base est configurable lors
    de l'installation.
-   Version 1.45 (22/07/2011) : Url absolu dans l'upload.
-   Version 1.44 (21/07/2011) : Indentation du code.
-   Version 1.43 (21/07/2011) : Amélioration du système de plugin.
-   Version 1.42 (21/07/2011) : Correction et amélioration d'un bug lors
    du parsage d'url.
-   Version 1.41 (21/07/2011) : Lors de l'upload, accents et caractères
    spéciaux sont enlevé.
-   Version 1.40 (20/07/2011) : Le Flux RSS est valide, il y a les
    dates, et la description est possible dans le fichier config.php
    (le réinstaller).
-   Version 1.39 (20/07/2011) : Dans l'upload d'image, remplacement de
    width: 100% par max-width: 100%.
-   Version 1.38 (18/07/2011) : J'ai revu la durée du cache pour le
    faire durée moins longtemps qu'une heure (1/2h).
-   Version 1.37 (18/07/2011) : J'ai corriger un bug dans la connexion.
    Un point en trop -\_-'
-   Version 1.36 (18/07/2011) : correction du bug de la connexion SQL
    dans system.php
-   Version 1.35 (18/07/2011) : Déplacement de la connexion SQL dans
    system.php
-   Version 1.34 (17/07/2011) : regroupement des variables de
    connections dans config.php. On peut definir le login et le mot de
    passe de l'administration sont a présent configurable dans
    l'installation
-   [Version 1.33](archives/v133.zip) (15/07/2011) : Correction d'un bug
    dans l'upload.
-   Version 1.32 (12/07/2011) : ID des news a partir de 1.
-   Version 1.31 (12/07/2011) : Parsage automatique des URL.
-   Version 1.30 (03/07/2011) : Correction d'un bug dans
    concernant l'installation.
-   Version 1.29 (03/07/2011) : Correction d'un bug dans concernant le
    define message.
-   Version 1.28 (30/06/2011) : Correction d'un bug dans l'installation.
-   Version 1.27 (30/06/2011) : Correction d'un bug le flux RSS.
-   Version 1.26 (30/06/2011) : Correction d'un bug dans les includes.
-   Version 1.25 (29/06/2011) : Amélioration du design
    de l'administration.
-   [Version 1.24](archives/v124.zip) (21/06/2011) : Ajout d'un système
    de login/mots de passe sur le site.
-   Version 1.23 (28/05/2011) : Ajout stripslashes() sur le titre.
-   Version 1.22 (19/05/2011) : Correction bug du système de plugins.
-   Version 1.21 (18/05/2011) : Ajout d'un système de plugins.
-   Version 1.20 (18/05/2011) : Ajout de l'attribut HTML5 "Required"
    dans la rédaction des news. Correction d'un petit bug sur l'encodage
    dans l'installation (Méta).
-   Version 1.19 (04/05/2011) : Mise en place d'un moteur de recherche.
-   Version 1.18 (02/05/2011) : Mise en place d'une adresse "fixe" pour
    les news dans le flux RSS.
-   Version 1.17 (01/05/2011) : Mise en place d'une adresse "fixe" pour
    les news. Pratique pour les back-links.
-   Version 1.16 (28/04/2011) : La template ne marche pas, donc retour à
    l'ancien système pour le design, je l'enlève. Le cache est mis à
    1 minute. La première fois, il y a une redirection vers le dossier
    d'installation
-   Version 1.15 (28/04/2011) : Correction bug dans le fichier connexion
    apparu dans la version 1.13.
-   Version 1.14 (27/04/2011) : Correction problème encodage
-   Version 1.13 (26/04/2011) : Convertit fichier + BDD en UTF-8
    pour l'internationalisation. Correction du bug des images
    dans l'administration. Sauvegardez votre base de donnée et
    réinstallez là avec l'aide du dossier install/
-   Version 1.12 (26/04/2011) : Traduction du fichier upload.php
-   Version 1.11 (10/04/2011) : Corrections de bugs divers
-   Version 1.10 (03/04/2011) : Renommage lors de l'upload
-   Version 1.09 (27/03/2011) : Mise en place d'icônes dans
    l'administration
-   Version 1.08 (12/03/2011) : modification code de rendu pour
    l'upload d'image. Ajout du alt="" + style="width: 100%;"
-   Version 1.07 (09/03/2011) : système de cache dans l'affichage des
    news + supression de la page d'affichage des news non paginée.
-   Version 1.06 (01/03/2011) : Insertion du n° de la nouvelle version
-   Version 1.05 (06/02/2011) : J'enlève l'éditeur WYSIWYG TinyEditor
    pour cause de bug.
-   Version 1.05 (05/02/2011) : Mise en place de l'éditeur WYSIWYG
    TinyEditor
-   Version 1.04 (24/01/2011) : Mise en place du systeme de template
    pour la partie visiteur du site.
-   Version 1.03 (18/01/2011) : Correction d'un bug dans le
    fichier upload.php.
-   Version 1.02 (01/01/2011) : J'enleve la fonction des liens
    cliquables, ça ne marche pas. Résolution problème d'encodage du
    fichier upload.php.
-   Version 1.01 (31/12/2010) : Corrections de erreurs php dans
    newspaginer.php
-   [Version 1.0](archives/v1.zip) (29/12/2010) : création

