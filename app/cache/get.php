<?php 
/*
http://www.example.org/cache/get.php?g={md5_from_email}
*/
if(!is_dir("avatar")) { mkdir("avatar");}

$expire = time() -604800 ;  
if (isset($_GET['g']))  {
	if (strlen($_GET['g']) !== 32) { 
		die; 
	}  
	$hash = preg_replace("#[^a-f0-9]#", "", $_GET['g'] );  
	if (strlen($hash) !== 32) { 
		die; 
	}  
	$newfile = 'avatar/'.$hash.'.png';
	$file = 'https://secure.gravatar.com/avatar/'.$hash.'?s=60&d=retro';
	if(file_exists($newfile) && filemtime($newfile) < $expire) {
		unlink($newfile); 
	} 
	if (!file_exists($newfile)) {
		copy($file, $newfile);  
		$imagecheck = getimagesize($newfile);
		if ($imagecheck['mime']!=='image/png')  {   
			imagepng(imagecreatefromjpeg($newfile),$newfile.'2');  
			unlink($newfile);
			rename($newfile.'2', $newfile);
		}
	}
	header('Location: '.$newfile.''); 
} 
else{ 
	header("HTTP/1.0 404 Not Found"); 
	echo "Error"; 
}
?>

