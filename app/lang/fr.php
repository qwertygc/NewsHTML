<?php
$termes = array();

$termes['modify'] = 'Modifier';
$termes['remove'] = 'Supprimer';
$termes['title'] = 'Titre';
$termes['date'] = 'Date';
$termes['content'] = 'Contenu';
$termes['envoyer'] = 'Envoyer';
$termes['backliste'] = 'Liste des articles';
$termes['upload'] = 'Télécharger';
$termes['uploaderreur'] = 'Impossible de copier le fichier dans';
$termes['addnews'] = 'Ajouter un article';
$termes['notimage'] = 'Le fichier n\'est pas une image';
$termes['maj'] = 'Mise à jour';
$termes['newversion'] = 'Il y a une nouvelle version';
$termes['download'] = 'La télécharger';
$termes['lastversion'] = 'Vous avez la dernière version';
$termes['filenotfound'] ='Le fichier est introuvable';
$termes['notimage'] ='Le fichier n\'est pas une image';
$termes['errorcopyupload'] ='Impossible de copier le fichier';
$termes['uploadok'] ='Le fichier a bien été téléchargé';
$termes['login'] = 'Identifiant';
$termes['password'] = 'Mot de passe';
$termes['error'] = 'Erreur';
$termes['logout'] = 'Déconnexion';
$termes['plugin'] = 'Plugins';
$termes['comment'] = 'Commentaires';
$termes['comment0'] = 'Commentaire';
$termes['pseudo'] = 'Pseudonyme';
$termes['email'] = 'Courriel';
$termes['website'] = 'Site';
$termes['message'] = 'Message';
$termes['notifmail'] = 'Notifications  par courriel';
$termes['noid'] = 'L’identifiant de l\'article n’est pas d&eacute;fini.';
$termes['thinkcomment'] = "Merci d'avoir commenté !";
$termes['tag'] = "étiquette";
$termes['cookie'] = "Retenir ces informations avec un cookie ?";
$termes['config'] = "Configuration";
$termes['sqlconnexion'] = "Connexion SQL";
$termes['hote'] = "Hôte";
$termes['database'] = "Base de données";
$termes['lang'] = "langue";
$termes['root'] = "Répertoire de base";
$termes['typeofdb'] = "Type de base de donnée";
$termes['style'] = "Design";
$termes['title'] = "title";
$termes['description'] = "Description";
$termes['nb_message_page'] = "Nombre d'articles par page";
$termes['text_resume'] = "Résumé de l'article";
$termes['text_resume_nb'] = "Nombre de mots du résumé";
$termes['prefix'] = "Préfixe";
$termes['draft'] = "brouillon";
$termes['name'] = "Nom";
$termes['extension'] = "Extension";
$termes['size'] = "Poids";
$termes['notfound'] = "Aucun résultat trouvé";
$termes['dateformat'] = "JJ/MM/AAAA/hh/mm/ss";
$termes['news'] = "article(s)";
$termes['validationcomment'] = "commentaire en cours de validation";
$termes['validation'] = "validation";
$termes['moderation'] = "Validation des commentaires à priori";
$termes['format_date'] = "Format de la date";
$termes['debug'] = "Activer le deboggage";
$termes['tag_separator'] = "séparateur d'étiquette";
$termes['captcha'] = "CAPTCHA";
$termes['typecaptcha'] = "écrivez le mot ".CAPTCHA;
$termes['authorlist'] = "Liste des rédacteurs";
$termes['author'] = "Rédacteur";
$termes['admin'] = "Administrateur";
$termes['statut'] = "statut";
$termes['addauthor'] = "Ajouter un rédacteur";
$termes['readingtime'] = "Temps de lecture estimé :";
$termes['format_url'] = "Personnalisation des URL";
$termes['format_url_post'] = "URL des articles";
$termes['format_url_author'] = "URL des auteurs";
$termes['format_url_tag'] = "URL des étiquettes";
$termes['format_url_pagination'] = "URL de la pagination";
$termes['format_parse'] = "Syntaxe :";
$termes['maintenance'] = "Maintenance";
$termes['backup'] = "Sauvegarde";
$termes['backup_sql'] = "Sauvegarde de la base de donnée";
$termes['backup_files'] = "Sauvegarde des fichiers";
$termes['automatically_update'] = "Mise à jour automatique";
$termes['enable_cache'] = "Activer le cache";
$termes['link'] = "Lien";
?>