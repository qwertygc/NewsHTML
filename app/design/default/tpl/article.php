<article class="h-entry">
	<header>
		<?php if(isset($_GET['id'])) {?><h1 class="p-name"><?php echo $title;?></h1><?php } else { ?><h1 class="p-name"><a href="<?php echo url_format($id,FORMAT_URL_POST);?>"><?php echo $title;?></a></h1><?php } ?>
		<time datetime="<?php echo $datetime;?>" class="dt-published"><?php echo $date;?></time> <span class="p-author h-card" rel="author"><?php echo $author; ?></span>
		<a href="?id=<?php echo $id;?>#comment" class="nbcomment"><?php echo $nbcomment;?></a>
		<?php echo $readingtime; ?>
		<?php if(!empty($link)) { ?><span class="link">&#9875;<a href="<?php echo $link; ?>"><?php echo $link; ?></a></span><?php } ?>
	</header>
	<div class="e-content">
		<?php echo $content;?>
	</div>
	<aside>
		<span id="tags" class="p-category"><?php echo $tags;?></span>
	</aside>
</article>
	