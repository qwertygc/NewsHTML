<!DOCTYPE html>
<html lang="<?php echo LANG; ?>">
<head>
  <title><?php echo stripslashes(TITLE).' '.@subtitle($subtitle); ?></title>
  <link rel="stylesheet" media="screen, handheld, tv, projection" type="text/css" href="<?php echo 'app/design/'.DESIGN.'/style.css'; ?>"/>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <link rel="icon" type="image/png" href="<?php echo 'app/design/'.DESIGN.'/img/favicon.css'; ?>" />
  <link rel="alternate" type="application/atom+xml" title="ATOM" href="feed.php" />
  <?php if(isset($_GET['id'])) {echo '<link rel="alternate" type="application/rss+xml" title="ATOM '.translate('comment').'" href="feed.php?comment='.$_GET['id'].'" />';} ?>
</head>
<body>
  <header id="header">
 <h1><a href="index.php"><?php echo stripslashes(TITLE); ?></a></h1>
 <p><?php echo stripslashes(DESCRIPTION); ?></p>
  </header>
  <nav id="speedbar">
		<ul>
			<li><a href="#">Item 1</a></li>
			<li><a href="#">Item 2</a></li>
			<li><a href="#">Item 3</a></li>

		</ul>
		<div class="recherche">
			<form action="search.php" method="get">
			<input type="search" name="q">
			<input type="submit">
			</form>
		</div>
  </nav>
  <aside id="aside">
  <h3>Title 1</h3>
  <ul>
	<li><a href="#">Item 1</a></li>
	<li><a href="#">Item 2</a></li>
  </ul>
    <h3>Title 2</h3>
  <ul>
	<li><a href="#">Item 1</a></li>
	<li><a href="#">Item 2</a></li>
  </ul>
  </aside>
  <div id="corps">