<?php
if(extension_loaded('zlib')){ob_start('ob_gzhandler');}
session_start();
include 'app/inc/system.php';
include 'app/design/'.DESIGN.'/tpl/header.php';
debug('top');
if(isset($_COOKIE['auth']) && !isset($_SESSION['login'])) {
	$auth = $_COOKIE['auth'];
	$auth = explode('---', $auth);
	$something = $bdd->prepare("SELECT * FROM ".PREFIX."author WHERE id_author =?");
	$something->execute(array($auth[0]));
	$data = $something->fetch();
	$key =$data['id_author'].'---'.sha1($data['pseudo'].$data['password']);
	if ($key == $_COOKIE['auth']) {
		$_SESSION['login'] = $data['pseudo'];
		$_SESSION['privilege'] = $data['privilege'];
		$_SESSION['author_id'] = $data['id_author'];
		$_SESSION['token'] = md5(uniqid(rand(), true));
		ban_loginOk();
		mylog($_SERVER['REMOTE_ADDR'].' login successful');
	}
	else {
		setcookie('auth', false, time() + 365*24*3600, null, null, false, true);
	}
}
//Partie de vérification des identifiants
if(isset($_GET['check'])){
	switch ($_GET['check']){
		case 'check':
			usleep(10000);
			if(isset($_POST['login']) && isset($_POST['password'])) {
				$something = $bdd->prepare("SELECT * FROM ".PREFIX."author WHERE pseudo =? AND password=?");
				$something->execute(array($_POST['login'], hash("sha512", $_POST['password'].SALT)));
				$data = $something->fetch();
				debug('query');
				if ($data == true and ban_canLogin() == true) {
					$_SESSION['login'] = $data['pseudo'];
					$_SESSION['privilege'] = $data['privilege'];
					$_SESSION['author_id'] = $data['id_author'];
					$_SESSION['token'] = md5(uniqid(rand(), true));
					ban_loginOk();
					mylog($_SERVER['REMOTE_ADDR'].' login successful');
					if(isset($_POST['cookie'])) {setcookie('auth', $data['id_author'].'---'.sha1($data['pseudo'].$data['password']), time() + 365*24*3600, null, null, false, true);}
					redirect('admin.php');
				}
				else {
					echo translate('error');
					if(ban_canLogin()) {mylog($_SERVER['REMOTE_ADDR'].' login failed for user '.$_POST['login'].' and password '.$_POST['password'].'IP IS BANNED'); }
					else {mylog($_SERVER['REMOTE_ADDR'].' login failed for user '.$_POST['login'].' and password '.$_POST['password'].'');}
					usleep(20000);
					ban_loginFailed();
					// redirect('admin.php'); 
					die();					
				}
			}
		break;
	}
}
if(isset($_SESSION['login'])) {
	echo '<ul id="tabs">'.PHP_EOL;
		echo "\t\t".'<li>'.tab_admin('', translate('backliste')).'</li>'.PHP_EOL;
		echo "\t\t".'<li>'.tab_admin('addnews',translate('addnews')).'</li>'.PHP_EOL;
		echo "\t\t".'<li>'.tab_admin('upload',translate('upload')).'</li>'.PHP_EOL;
		if ($comment) {echo '<li>'.tab_admin('comment',translate('comment')).'</li>'.PHP_EOL; }
		echo '<li>'.tab_admin('author',translate('authorlist')).'</li>'.PHP_EOL;
		echo "\t\t".'<li>'.tab_admin('plugin',translate('plugin')).'</li>'.PHP_EOL;
		if($_SESSION['privilege']) {echo "\t\t".'<li>'.tab_admin('maintenance',translate('maintenance')).'</li>'.PHP_EOL;}
		if($_SESSION['privilege']) {echo "\t\t".'<li>'.tab_admin('config',translate('config')).'</li>'.PHP_EOL;}
		echo "\t\t".'<li>'.tab_admin('logout',translate('logout')).'</li>'.PHP_EOL;
	echo '</ul>'.PHP_EOL;
if(isset($_GET['action'])){
	switch ($_GET['action']) {
		case 'logout':
//si on se deconnecte, on détruit les sessions
			$_SESSION = array();
			session_destroy();
			if(isset($_COOKIE['auth'])) {setcookie('auth', false, time() + 365*24*3600, null, null, false, true);}
			redirect('admin.php'); 
		break;
		case 'addnews':
//Partie d'édition d'une news
			if (isset($_GET['modify_news'])) {
				$_GET['modify_news'] = htmlspecialchars($_GET['modify_news']);
				$something = $bdd->prepare('SELECT * FROM '.PREFIX.'news WHERE id=?');
				debug('query');
				$something->execute(array(intval($_GET['modify_news'])));
				$something->setFetchMode(PDO::FETCH_BOTH);
				$data = $something->fetch();
				$title = $data['title'];
				$link = $data['link'];
				$tag = $data['tag'];
				$content = $data['content'];
				$id_news = $data['id']; 
				$affcomment = $data['affcomment'];
				$draft = $data['draft'];
				$private = $data['private'];
				$date = date('d/m/Y/H/i/s', $data['timestamp']);
				$id_author = $data['author_id'];
			}
			else {
				$ifexists = $bdd->prepare('SELECT count(link), id from '.PREFIX.'news WHERE link=? ORDER BY timestamp DESC');
				$ifexists->execute(array($_GET['link']));
				$ifexists_data = $ifexists->fetch();
				if($ifexists_data['count(link)'] != 0) {redirect('admin.php?modify_news='.$ifexists_data['id'].'&action=addnews');}
				debug('query');
				$title = (!empty($_GET['link'])) ? get_title_url($_GET['link']) : '';
				$link = (!empty($_GET['link'])) ? $_GET['link'] : '';
				$content = '';
				$tag ='';
				$date= date('d/m/Y/H/i/s',time());
				$id_news = 0;
				$draft='0';
				$private='';
				$affcomment = ($comment == true) ? '1' : '0';
				$id_author = $_SESSION['author_id'];
			}
			if(isset($_SESSION['privilege'])) {
				$something = $bdd->query('SELECT id_author, name FROM '.PREFIX.'author');
				$something->setFetchMode(PDO::FETCH_BOTH);
				while($data_author = $something->fetch()) {$authors[$data_author['id_author']] = $data_author['name'];}
			}
			## Form redaction
			$form = New form(array('action'=>'admin.php', 'method'=>'post'));
			$form->label('title',translate('title'));
			$form->input(array('type'=>'text', 'size'=>'30', 'name'=>'title', 'value'=>$title, 'required'=>'required', 'spellcheck'=>'true', 'placeholder'=>translate('title')));
			tpl_include('app/design/'.DESIGN.'/tpl/toolbar.php');
			$form->label('textarea', translate('content'));
			$form->textarea(array('id'=>'textarea', 'name'=>'content', 'cols'=>'50', 'rows'=>'10', 'required'=>'required', 'autofocus'=>'autofocus',  'placeholder'=>translate('content')), $content);
			$form->label('tag',translate('tag'));
			$form->input(array('type'=>'text', 'size'=>'30', 'name'=>'tag', 'value'=>$tag, 'spellcheck'=>'true', 'autofocus'=>'autofocus', 'placeholder'=>translate('tag')));
			$form->label('date', translate('date').' ('.translate('dateformat').')');
			$form->input(array('type'=>'text', 'size'=>'30', 'name'=>'date', 'value'=>$date, 'pattern'=>'[0-9]{2}/[0-9]{2}/[0-9]{4}/[0-9]{2}/[0-9]{2}/[0-9]{2}', 'placeholder'=>translate('dateformat')));
			$form->label('link',translate('link'));
			$form->input(array('type'=>'url', 'size'=>'30', 'name'=>'link', 'value'=>$link, 'placeholder'=>'http://example.org/foo/bar'));
			if($_SESSION['privilege'] == 1) {
				$form->label('author_id', translate('author'));
				$form->select(array('name'=>'author_id'), $authors, (int)$id_author, true);
			}
			$form->label('private',translate('password'));
			$form->input(array('type'=>'text', 'size'=>'30', 'name'=>'private', 'value'=>$private));
			if($comment == true) {
			$checked_affcomment = ($affcomment =="1") ? true:false;
				$form->label('affcomment', translate('comment'));
				$form->checkbox(array('type'=>'checkbox', 'name'=>'affcomment', 'value'=>'affcomment'), $checked_affcomment);
			}
			$draft_checked = ($draft =="1") ? true:false;
			$form->label('draft', translate('draft'));
			$form->checkbox(array('type'=>'checkbox', 'name'=>'draft', 'value'=>'draft'), $draft_checked);
			$form->input(array('type'=>'hidden', 'name'=>'id_news', 'value'=>$id_news));
			$form->input(array('type'=>'submit'));
			$form->endform();
		break;
		case'upload':
			$form = New form(array('method'=>'post', 'enctype'=>'multipart/form-data', 'action'=>'?action=upload'));
			$form->input(array('type'=>'file', 'name'=>'file[]', 'size'=>'30', 'multiple'=>'multiple'));
			$form->input(array('type'=>'submit', 'name'=>'upload', 'value'=>translate('upload')));
			$form->endform();

		if(isset($_POST['upload']) && DEMO !== true) {
				$files = array();
				$fdata = $_FILES['file'];
				$count = count($fdata['name']);
				if(is_array($fdata['name'])){
					for($i=0;$i<$count;++$i){
						$files[]=array(
							'name'     => $fdata['name'][$i],
							'tmp_name' => $fdata['tmp_name'][$i],
							'type' => $fdata['type'][$i],
						);
					}
				}
				else {$files[]=$fdata;}
				foreach ($files as $file) {
					if(!is_uploaded_file($file['tmp_name'])) {exit(translate('filenotfound'));}
					$nb_img = file_get_contents('data/upload/upload.txt');
					$file['name']=$nb_img.$file['name'];
					++$nb_img;
					file_put_contents('data/upload/upload.txt', $nb_img);
					$file['name']= stripAccents($file['name']);
					if(!move_uploaded_file($file['tmp_name'], 'data/upload/'. $file['name']) ) {exit(translate('errorcopyupload'));}
					echo translate('uploadok');
					echo inputurl(''.ROOT.'/data/upload/'.$file['name'].'');	
				}
			}
		if (isset($_GET['delete']) && !empty($_SESSION['token']) && $_SESSION['token']==$_GET['token'] && DEMO !== true) {
			if (file_exists('data/upload/'. $_GET['delete'])) {
				delete_file('data/upload/'.$_GET['delete'].'');
				redirect('admin.php?action=upload'); 
			}
			else {
				echo translate('filenotfound');
			}
		}
		if (!empty($_GET['edit'])) {
			if (file_exists('data/upload/'. $_GET['edit'])) {
				$file = pathinfo($_GET['edit']);
				echo inputurl(''.ROOT.'/data/upload/'.$_GET['edit'].'');
				echo '<p>'.translate('name').': '.$file['basename'].'</p>';
				echo '<p>'.translate('extension').': '.$file['extension'].'</p>';
				echo '<p>'.translate('size').': '.octectsconvert(filesize('data/upload/'.$_GET['edit'])).'</p>';
				echo '<p>'.translate('date').': '.format_date(FORMAT_DATE,filemtime('data/upload/'. $_GET['edit'])).'</p>';
				echo '<p>'.translate('extension').': '.$file['extension'].'</p>';
				echo '<p>sha1: '.sha1('data/upload/'. $_GET['edit']).'</p>';
				echo '<p>md5: '.md5_file('data/upload/'. $_GET['edit']).'</p>';
			}
		}
		$dir= scandir('data/upload/');
		echo '<table>'.PHP_EOL;
		echo '<tr><th>'.translate('remove').'</th><th>'.translate('name').'</th><th>'.translate('extension').'</th><th>'.translate('size').'</th><th>'.translate('date').'</th></tr>';
		$root = ROOT.'/data/upload/';
		foreach($dir as $file) {		
			if($file !='.' AND $file !='..' AND $file !='upload.txt') {
				$f = pathinfo($file);
				echo '<tr>';
				echo '<td><a href="admin.php?action=upload&edit='.$file.'"><img src="app/design/modify.png" alt="'.translate('modifier').'" /></a><a href="admin.php?action=upload&delete='.$file.'&token='.$_SESSION['token'].'"><img src="app/design/remove.png" alt="'.translate('remove').'" /></a></td>'.PHP_EOL;
				echo '<td><a href="data/upload/'.$file.'">'.$root.$file.'</td>';
				echo '<td>'.$f['extension'].'</td>';
				echo '<td>'.octectsconvert(filesize('data/upload/'.$file)).'</td>';
				echo '<td>'.format_date(FORMAT_DATE,filemtime('data/upload/'. $file)).'</td>';
				
				echo '</tr>';
			}
		}
		echo '</table>';
		break;
		case 'comment':	
		if ($comment == true) { 
			echo '<h1>'.translate('comment').'</h1>'.PHP_EOL;
			if (isset($_GET['delete_comment']) && !empty($_SESSION['token']) && $_SESSION['token']==$_GET['token'] && DEMO !== true) {
				sql('DELETE FROM '.PREFIX.'comment WHERE id=?', array($_GET['delete_comment']));
				delete_file('app/cache/tmp/article/'.$_GET['idnews'].'.html');
				push_pshb();
			}
			if (isset($_GET['validate_comment']) && !empty($_SESSION['token']) && $_SESSION['token']==$_GET['token']  && DEMO !== true) {
				sql('UPDATE '.PREFIX.'comment SET moderation=1 WHERE id=?', array($_GET['validate_comment']));
				delete_file('app/cache/tmp/article/'.$_GET['idnews'].'.html');	
				push_pshb();
			}
			if (isset($_GET['unvalidate_comment']) && !empty($_SESSION['token']) && $_SESSION['token']==$_GET['token'] &&  DEMO !== true) {
				sql('UPDATE '.PREFIX.'comment SET moderation=0 WHERE id=?', array($_GET['unvalidate_comment']));
				delete_file('app/cache/tmp/article/'.$_GET['idnews'].'.html');
				push_pshb();
			}
			$something = ($_SESSION['privilege']) ? $bdd->query('SELECT count(id) FROM '.PREFIX.'comment') : $bdd->query('SELECT count(c.id) FROM comment c, news n, author a WHERE c.idnews = n.id AND a.id_author = n.author_id AND a.id_author ="'.$_SESSION['author_id'].'"');
			$data = $something->fetch();
			//On compte le nombre de news, et s'il y en a plus de 10, ont fait une autre page.
			$nb_messages_pages = NB_MESSAGES_PAGE*10;
			$nbPages = ceil($data['count(id)'] / $nb_messages_pages);
			$page = 0;
			if(isset($_GET['page']) && (intval($_GET['page']) <= $nbPages)) {
				$page = intval($_GET['page']) - 1;
			}
			else {$_GET['page'] = 1;}
			echo '<table>'.PHP_EOL;
			echo "\t\t".'<tr>'.PHP_EOL;
			echo "\t\t".'<th>'.translate('news').'</th>'.PHP_EOL;
			echo "\t\t".'<th>'.translate('validation').'</th>'.PHP_EOL;
			echo "\t\t"."\t\t".'<th>'.translate('remove').'</th>'.PHP_EOL;
			echo "\t\t"."\t\t".'<th>'.translate('pseudo').'</th>'.PHP_EOL;
			echo "\t\t"."\t\t".'<th>'.translate('comment').'</th>'.PHP_EOL;
			echo "\t\t"."\t\t".'<th>'.translate('email').'</th>'.PHP_EOL;
			echo"\t\t". "\t\t".'<th>'.translate('date').'</th>'.PHP_EOL;
			echo "\t\t".'</tr>'.PHP_EOL;
			if($_SESSION['privilege']) {
				$something = $bdd->query('SELECT * FROM '.PREFIX.'comment ORDER BY id DESC LIMIT '.$nb_messages_pages * $page.', '.$nb_messages_pages);
			}
			else {
				$something = $bdd->prepare('SELECT c.* FROM '.PREFIX.'comment c, '.PREFIX.'author a, '.PREFIX.'news n WHERE c.idnews = n.id AND a.id_author = n.author_id AND a.id_author = ? LIMIT '.$nb_messages_pages * $page.', '.$nb_messages_pages);
				$something->execute(array($_SESSION['author_id']));
			}
			$something->setFetchMode(PDO::FETCH_BOTH);
			debug('query');
			while($data = $something->fetch()) {
				echo "\t\t".'<tr>';
				echo "\t\t"."\t\t".'<td><a href="'.url_format($data['idnews'],FORMAT_URL_POST).'#'.$data['id'].'">'.'#'.$data['idnews'].'</a>'.'</td>'.PHP_EOL;
					echo "\t\t".'<td>';
					if($data['moderation'] == "0") {
						echo '<a href="?validate_comment='.$data['id'].'&action=comment&token='.$_SESSION['token'].'&idnews='.$data['idnews'].'"><img src="app/design/validation.png" alt="'.translate('validation').'"/></a>'.'';
					}
					if($data['moderation'] == "1") {
						echo '<a href="?unvalidate_comment='.$data['id'].'&action=comment&token='.$_SESSION['token'].'&idnews='.$data['idnews'].'"><img src="app/design/remove.png" alt="'.translate('validation').'"/></a>'.'';
					}
				echo '</td>';
			
				echo "\t\t"."\t\t".'<td><a href="?delete_comment='.$data['id'] .'&action=comment&token='.$_SESSION['token'].'&idnews='.$data['idnews'].'"><img src="app/design/remove.png" alt="'.translate('remove').'"/></a>'.'</td>'.PHP_EOL;
				echo "\t\t"."\t\t".'<td>'.$data['pseudo'].'</a></td>'.PHP_EOL;
				echo "\t\t"."\t\t".'<td>'.$data['message'].'</td>'.PHP_EOL;
				echo "\t\t"."\t\t".'<td>'.$data['email'].'</td>'.PHP_EOL;
				echo "\t\t"."\t\t".'<td>'.format_date(FORMAT_DATE, $data['timestamp']).'</td>'.PHP_EOL;
				echo "\t\t".'</tr>'.PHP_EOL;
			}
			echo '</table>';
			$pagination = pagination($_GET['page'], $nbPages, 'admin.php?action=comment&page=%d');
			if(tpl_include('app/design/'.DESIGN.'/tpl/pagination.php')) {} else { echo '<div id="pagination">'.$pagination.'</div>'; }

			}
			break;
			case 'author':
				$something = ($_SESSION['privilege']) ? $bdd->query('SELECT * FROM '.PREFIX.'author ORDER BY id_author DESC') : $bdd->query('SELECT * FROM '.PREFIX.'author WHERE id_author ="'.$_SESSION['author_id'].'"');
				$something->setFetchMode(PDO::FETCH_BOTH);
				debug('query');
				echo '<table>'.PHP_EOL;
				echo "\t\t".'<tr>'.PHP_EOL;
				echo "\t\t"."\t\t".'<th>'.translate('modify').'</th>'.PHP_EOL;
				if($_SESSION['privilege']) {echo "\t\t"."\t\t".'<th>'.translate('remove').'</th>'.PHP_EOL;}
				echo "\t\t"."\t\t".'<th>'.translate('name').'</th>'.PHP_EOL;
				echo "\t\t"."\t\t".'<th>'.translate('pseudo').'</th>'.PHP_EOL;
				echo "\t\t"."\t\t".'<th>'.translate('statut').'</th>'.PHP_EOL;
				echo "\t\t".'</tr>'.PHP_EOL;
				while($data = $something->fetch()) {
					$statut = ($data['privilege'] == "1") ? translate('admin') : translate('author');
					echo "\t\t".'<tr>';
					echo "\t\t"."\t\t".'<td><a href="admin.php?modify_author='.$data['id_author'].'&action=author"><img src="app/design/modify.png" alt="'.translate('modify').'"></a></td>'.PHP_EOL;
					if($_SESSION['privilege']) {echo "\t\t"."\t\t".'<td><a href="admin.php?delete_author='.$data['id_author'].'&action=author&token='.$_SESSION['token'].'"><img src="app/design/remove.png" alt="'.translate('remove').'"></a></td>'.PHP_EOL;}
					echo "\t\t"."\t\t".'<td>'.$data['name'].'</td>'.PHP_EOL;
					echo "\t\t"."\t\t".'<td>'.$data['pseudo'].'</td>'.PHP_EOL;
					echo "\t\t"."\t\t".'<td>'.$statut.'</td>'.PHP_EOL;
					echo "\t\t".'</tr>';
				}
				echo '</table>';
			if (isset($_GET['modify_author'])) {
				$something = $bdd->prepare('SELECT * FROM '.PREFIX.'author WHERE id_author=?');
				debug('query');
				$something->execute(array(intval($_GET['modify_author'])));
				$something->setFetchMode(PDO::FETCH_BOTH);
				$data = $something->fetch();
				$name = $data['name'];
				$pseudo = $data['pseudo'];
				$id_author = $data['id_author']; 
				$privilege = $data['privilege']; 
			}
			else {
				$name = '';
				$pseudo = '';
				$privilege = '0';
				$id_author = 0;
			}
				## Form redaction
				if(isset($_GET['modify_author']) || $_SESSION['privilege'] == 1) {
					echo '<h1>'.translate('addauthor').'</h1>';
					$form = New form(array('action'=>'admin.php?action=author', 'method'=>'post'));
					$form->label('name',translate('name'));
					$form->input(array('type'=>'text', 'size'=>'30', 'name'=>'name', 'value'=>$name, 'required'=>'required', 'spellcheck'=>'true', 'autofocus'=>'autofocus', 'placeholder'=>translate('name')));
					$form->label('pseudo',translate('pseudo'));
					$form->input(array('type'=>'text', 'size'=>'30', 'name'=>'pseudo', 'value'=>$pseudo, 'required'=>'required', 'spellcheck'=>'true', 'autofocus'=>'autofocus', 'placeholder'=>translate('pseudo')));
					$form->label('password',translate('password'));
					$form->input(array('type'=>'password', 'size'=>'30', 'name'=>'password', 'spellcheck'=>'true', 'autofocus'=>'autofocus'));
					$checked_privilege = ($privilege == '1') ? true : false;
					if($_SESSION['privilege']) {$form->label('privilege', translate('admin'));
					$form->checkbox(array('type'=>'checkbox', 'name'=>'privilege'), $checked_privilege);}
					$form->input(array('type'=>'hidden', 'name'=>'id_author', 'value'=>$id_author));
					$form->input(array('type'=>'submit'));
					$form->endform();
				}
				if(isset($_GET['delete_author']) && !empty($_SESSION['token']) && $_SESSION['token']==$_GET['token'] && $_SESSION['privilege'] && DEMO !== true) {
					$something = $bdd->prepare('SELECT n.id FROM '.PREFIX.'news n, '.PREFIX.'author a  WHERE n.author_id =? AND n.author_id = a.id_author ORDER BY n.timestamp DESC');
					$something->execute(array($_GET['delete_author']));
					while($data = $something->fetch()) {
						echo $data['id'];
						sql('DELETE FROM '.PREFIX.'comment WHERE idnews=?', array($data['id']));
						delete_file('app/cache/tmp/article/'.$data['id'].'.html');	
					}
					sql('DELETE FROM '.PREFIX.'author WHERE id_author=?', array($_GET['delete_author']));
					sql('DELETE FROM '.PREFIX.'news WHERE author_id=?', array($_GET['delete_author']));
					redirect('admin.php?action=author');
				}
				if ((isset($_POST['id_author']) AND isset($_POST['pseudo']) && ($_SESSION['privilege'] == 1 || $_SESSION['author_id'] == $_POST['id_author'])) && DEMO !== true){
					$something = $bdd->prepare('SELECT id_author FROM author WHERE pseudo = :pseudo');
					$something->execute(array('pseudo'=> $_POST['pseudo']));
					$member_exist=$something->fetch();
						$privilege = ($_POST['privilege'] == true) ? '1':'0';
						if ($_POST['id_author'] == 0 && !$member_exist){ 
							sql('INSERT INTO '.PREFIX.'author(pseudo, name, password, privilege) VALUES(?, ?, ?, ?)', array($_POST['pseudo'],$_POST['name'],hash("sha512", $_POST['password'].SALT),$privilege));
						}
						else {
							$_POST['id_author'] = intval($_POST['id_author']);
							if(!empty($_POST['password'])) {
								sql('UPDATE '.PREFIX.'author SET pseudo=?, name=?, password=?, privilege=? WHERE id_author=?', array($_POST['pseudo'],$_POST['name'],hash("sha512", $_POST['password'].SALT),$privilege, $_POST['id_author']));
							}
							else {
								sql('UPDATE '.PREFIX.'author SET pseudo=?, name=?, privilege=? WHERE id_author=?', array($_POST['pseudo'],$_POST['name'],$privilege, $_POST['id_author']));
							}
						}
						redirect('admin.php?action=author');
				}
			
			break;			
			case 'plugin':
			$plugins= scandir('app/plugin/');
			// print_r($plugins);
			echo "\t\t<ul>".PHP_EOL;
			foreach($plugins as $plugin) {		
				$array=explode('.php',$plugin);
				if($array[0] !='.' AND $array[0] !='..') {
					echo "\t\t\t<li><a href=\"?plugin=$plugin&action=plugin\">$array[0]</a></li>".PHP_EOL;
				}
			}
			echo "\t\t</ul>";

			if (isset($_GET['plugin'])) {
				if(file_exists('app/plugin/'. $_GET['plugin'])) {include 'app/plugin/'. $_GET['plugin'];}
				else {echo translate('filenotfound');}
			}
		
		break;
		case 'maintenance':
			echo '<h1>'.translate('maj').'</h1>';
			if(checkupdate()) {
					echo translate('newversion').' ('.VERSION.'→'.file_get_contents(NH_ROOT.'/lastversion.txt').')<br/><a href="'.NH_ROOT.'">'.translate('download').'</a> <a href="?action=maintenance&subaction=update">'.translate('automatically_update').'</a>';
				}
				else {
					echo translate('lastversion');
				}
				echo '<h1>'.translate('backup').'</h1>';
				echo '<ul>';
				echo '<li><a href="?action=maintenance&subaction=backup_sql">'.translate('backup_sql').'</a></li>';
				echo '<li><a href="?action=maintenance&subaction=backup_files">'.translate('backup_files').'</a></li>';
				echo '</ul>';
				if(isset($_GET['subaction'])) {
					switch($_GET['subaction']) {
						case 'backup_sql':
							if(SGBD == "sqlite") {redirect('data/data.sqlite'); }
							if(SGBD == "mysql") {redirect(backup_mysql());}
						break;
						case 'backup_files':
							$filename = date("Y-m-d", time())."_archive";
							if(!file_exists('data/'.$filename.'.zip')) {Zip('data/', 'data/'.$filename.'.zip'); }
							redirect('data/'.$filename.'.zip');
						break;
						case 'update':
							file_put_contents("newshtml.zip", file_get_contents(NH_ROOT."/newshtml.zip")); 
							$zip = new ZipArchive;
							if ($zip->open('newshtml.zip') === TRUE) {
								$zip->extractTo('./');
								$zip->close();
								@unlink('newshtml.zip');
								update_lib('app/inc/lib/Parsedown.php', 'https://raw.githubusercontent.com/erusev/parsedown/master/Parsedown.php');
								update_lib('app/inc/lib/ParsedownExtra.php', 'https://raw.githubusercontent.com/erusev/parsedown-extra/master/ParsedownExtra.php');
								update_lib('app/inc/lib/pubsubhubbubpublisher.php', 'https://raw.githubusercontent.com/pubsubhubbub/php-publisher/master/library/Publisher.php');
								update_lib('app/inc/atom.class.php', 'http://champlywood.free.fr/bidouilles/dl/atom.class.phps');
								update_lib('app/inc/form.class.php', 'http://champlywood.free.fr/bidouilles/dl/form.class.phps');
								} 
							else {
								echo translate('error');;
							}
						break;
					}
				}
		break;
		case 'config':
			if($_SESSION['privilege']) {
				$form = New form(array('action'=>'admin.php?action=config&traitement', 'method'=>'post'));
				$form->startfieldset();
				$form->legend(translate('config'));
				$form->label('title', translate('title'));
				$form->input(array('type'=>'text', 'name'=>'title', 'value'=> stripslashes(TITLE), 'placeholder'=>'My website'));
				$form->label('description', translate('description'));
				$form->input(array('type'=>'text', 'name'=>'description', 'value'=> stripslashes(DESCRIPTION), 'placeholder'=>'my website is amazing'));
				$form->label('design', translate('style'));
				scandir_list_dir('design', 'app/design/', DESIGN);
				$form->label('nb_message_page', translate('nb_message_page'));
				$form->input(array('type'=>'number', 'name'=>'nb_message_page', 'value'=>NB_MESSAGES_PAGE, 'placeholder'=>'42', 'min'=>'1'));
				$form->label('comment', translate('comment'));
				$form->checkbox(array('type'=>'checkbox', 'name'=>'comment', 'value'=>'comment'), $comment);
				$form->label('moderation', translate('moderation'));
				$form->checkbox(array('type'=>'checkbox', 'name'=>'moderation', 'value'=>'moderation'), MODERATION);
				$form->label('text_resume', translate('text_resume'));
				$form->checkbox(array('type'=>'checkbox', 'name'=>'text_resume', 'value'=>'text_resume'), TEXT_RESUME);
				$form->label('text_resume_nb', translate('text_resume_nb'));
				$form->input(array('type'=>'number', 'name'=>'text_resume_nb', 'value'=>TEXT_RESUME_NB, 'placeholder'=>'42', 'min'=>'1'));
				$form->label('format_date', translate('format_date'));
				echo form_format_date();
				$form->endfieldset();
				$form->startfieldset();
				$form->legend(translate('config'));
				$form->label('lang',translate('lang'));
				scandir_list_file('lang', 'app/lang', LANG);
				$form->label('set_locale', 'SET LOCALE');
				$form->input(array('type'=>'text', 'name'=>'set_locale', 'value'=>SET_LOCALE, 'placeholder'=>'en_GB'));
				$form->label('root', translate('root'));
				$form->input(array('type'=>'text', 'name'=>'root', 'value'=>ROOT, 'placeholder'=>'http://example.org'));
				$form->label('email', translate('email'));
				$form->input(array('type'=>'email', 'name'=>'email', 'value'=>EMAIL, 'placeholder'=>'no-reply@example.org'));
				$form->label('tag_separator', translate('tag_separator'));
				$form->input(array('type'=>'text', 'name'=>'tag_separator', 'value'=>TAG_SEPARATOR, 'placeholder'=>';'));
				$form->label('captcha', translate('captcha'));
				$form->input(array('type'=>'text', 'name'=>'captcha', 'value'=>CAPTCHA, 'placeholder'=>'any word'));
				$form->label('format_parse', translate('format_parse'));
				$form->select(array('name'=>'format_parse'),array('markdown'=>'markdown', 'html'=>'HTML'), FORMAT_PARSE, true);
				form_timezone();
				$form->label('debug', translate('debug'));
				$form->checkbox(array('type'=>'checkbox', 'name'=>'debug', 'value'=>'debug'), DEBUG);
				$form->label('enable_cache', translate('enable_cache'));
				$form->checkbox(array('type'=>'checkbox', 'name'=>'enable_cache', 'value'=>'enable_cache'), ENABLE_CACHE);
				$form->endfieldset();
				$form->startfieldset();
				$form->legend(translate('format_url'));
				$form->label('format_url_post', translate('format_url_post'));
				$form->input(array('type'=>'text', 'name'=>'format_url_post', 'value'=>FORMAT_URL_POST, 'placeholder'=>'index.php?id=%d'));
				$form->label('format_url_author', translate('format_url_author'));
				$form->input(array('type'=>'text', 'name'=>'format_url_author', 'value'=>FORMAT_URL_AUTHOR, 'placeholder'=>'search.php?author=%s'));
				$form->label('format_url_tag', translate('format_url_tag'));
				$form->input(array('type'=>'text', 'name'=>'format_url_tag', 'value'=>FORMAT_URL_TAG, 'placeholder'=>'search.php?tag=%s'));
				$form->label('format_url_pagination', translate('format_url_pagination'));
				$form->input(array('type'=>'text', 'name'=>'format_url_pagination', 'value'=>FORMAT_URL_PAGINATION, 'placeholder'=>'index.php?page=%s'));
				$form->endfieldset();
				$form->startfieldset();
				$form->legend(translate('sqlconnexion'));
				$form->label('sgbd', translate('typeofdb'));
				$form->select(array('name'=>'sgbd'),array('mysql'=>'MySQL', 'sqlite'=>'SQLite'), SGBD, true);
				$form->label('hote', translate('hote'));
				$form->input(array('type'=>'text', 'name'=>'hote', 'value'=>URL_DB, 'placeholder'=>'localhost'));
				$form->label('login', translate('login'));
				$form->input(array('type'=>'text', 'name'=>'login', 'value'=>LOGIN, 'placeholder'=>'root'));
				$form->label('mdp', translate('password'));
				$form->input(array('type'=>'password', 'name'=>'mdp', 'value'=>PASSWORD));
				$form->label('base', translate('database'));
				$form->input(array('type'=>'text', 'name'=>'base', 'value'=>TABLE, 'placeholder'=>'test'));
				$form->label('prefix', translate('prefix'));
				$form->input(array('type'=>'text', 'name'=>'prefix', 'value'=>PREFIX, 'placeholder'=>'nh_'));
				$form->endfieldset();
				$form->input(array('type'=>'submit', 'name'=>'submit'));
				$form->endform();

				if(isset($_GET['traitement']) && DEMO !== true){
					erased_dir('app/cache/tmp');
					if($_POST['tag_separator'] != TAG_SEPARATOR) {sql('UPDATE '.PREFIX.'news SET tag= REPLACE(tag,?,?)', array(TAG_SEPARATOR, $_POST['tag_separator']));}
					$format_url_post = (isset($_POST['format_url_post'])) ? $_POST['format_url_post'] : 'index.php?id=%d';
					$format_url_author = (isset($_POST['format_url_author'])) ? $_POST['format_url_author'] : 'search.php?author=%s';
					$format_url_tag = (isset($_POST['format_url_tag'])) ? $_POST['format_url_tag'] : 'search.php?tag=%s';
					$format_url_pagination = (isset($_POST['format_url_pagination'])) ? $_POST['format_url_pagination'] : 'index.php?page=%d';
					$comment = (isset($_POST['comment'])) ? 'true':'false';
					$debug = (isset($_POST['debug'])) ? 'true':'false';
					$enable_cache = (isset($_POST['enable_cache'])) ? 'true':'false';
					$moderation = (isset($_POST['moderation'])) ? 'true':'false';
					$text_resume = (isset($_POST['text_resume'])) ? 'true':'false';
					$text_resume_nb = (is_numeric(trim($_POST['text_resume_nb']))) ? trim($_POST['text_resume_nb']) : '100';
					$nb_message_page = (is_numeric(trim($_POST['nb_message_page']))) ? trim($_POST['nb_message_page']) : '10';
					$texte = '<?php
							define("URL_DB", "'.trim($_POST['hote']).'");
							define("LOGIN", "'.trim($_POST['login']).'");
							define("PASSWORD", "'.trim($_POST['mdp']).'");
							define("TABLE", "'.trim($_POST['base']).'");
							define("SGBD", "'.trim($_POST['sgbd']).'");
							define("LANG", "'.trim($_POST['lang']).'");
							define("SET_LOCALE", "'.trim($_POST['set_locale']).'");
							define("DESIGN", "'.trim($_POST['design']).'");
							define("EMAIL", "'.$_POST['email'].'");
							define("SALT", "'.SALT.'");
							define("TITLE", "'.trim($_POST['title']).'");
							define("DESCRIPTION", "'.trim($_POST['description']) .'");
							define("ROOT", "'. trim($_POST['root']) .'");
							define("FORMAT_URL_POST", "'. $format_url_post.'");
							define("FORMAT_URL_AUTHOR", "'. $format_url_author.'");
							define("FORMAT_URL_TAG", "'. $format_url_tag.'");
							define("FORMAT_URL_PAGINATION", "'. $format_url_pagination.'");
							define("FORMAT_PARSE", "'. $_POST['format_parse'] .'");
							define("TIMEZONE", "'. $_POST['timezone'] .'");
							define("FORMAT_DATE", "'. $_POST['format_date'] .'");
							define("DEBUG", '.$debug.');
							define("ENABLE_CACHE", '.$enable_cache.');
							$comment ='.$comment.';
							define("MODERATION", '. $moderation .');
							define("PREFIX", "'. $_POST['PREFIX'] .'");
							define("NB_MESSAGES_PAGE", '.$nb_message_page.');
							define("TEXT_RESUME", '.$text_resume.');
							define("TEXT_RESUME_NB", '.$text_resume_nb.');
							define("TAG_SEPARATOR", "'.$_POST['tag_separator'].'");
							define("CAPTCHA", "'.$_POST['captcha'].'");
							define("DEMO", false);
						?>';
					file_put_contents('data/config.php', $texte);
					redirect('admin.php?action=config'); 
				}
			}
		break; 
	}
}
else {
	$something = ($_SESSION['privilege']) ? $bdd->query('SELECT count(id) FROM '.PREFIX.'news') : $bdd->query('SELECT count(id) FROM '.PREFIX.'news WHERE author_id="'.$_SESSION['author_id'].'"');
	$data = $something->fetch();
	debug('query');
	echo ''. $data['count(id)'].' '.translate('news');
	if (isset($_POST['title']) AND isset($_POST['content']) && DEMO !== true){
		$date = (!empty($_POST['date']) and check_date_format($_POST['date'])) ? date_human_posix($_POST['date']) : time(); //On converti la chaine de date en format "humain" vers le format timestamp
		$affcomment = (isset($_POST['affcomment'])) ? "1" : "0";
		$draft = (isset($_POST['draft'])) ? "1" : "0";
		$private = $_POST['private'];
		$author_id= ($_SESSION['privilege']) ? $_POST['author_id'] : $_SESSION['author_id'];
		if ($_POST['id_news'] == 0){ //Ajout d'une nouvelle news
			sql('INSERT INTO '.PREFIX.'news(title, link, content, tag, affcomment, draft, private, author_id, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)', array($_POST['title'],$_POST['link'],$_POST['content'],$_POST['tag'],$affcomment,$draft, $private, $author_id,$date));
			erased_dir('app/cache/tmp/index');
			erased_dir('app/cache/tmp/feed');
			push_pshb();
		}
		else {
			$_POST['id_news'] = intval($_POST['id_news']);
			//Mise à jour de la news
			sql('UPDATE '.PREFIX.'news SET title=?, link=?, content=?, tag=?, affcomment=?, draft=?, private=?, author_id=?, timestamp=? WHERE id=?', array($_POST['title'],$_POST['link'], $_POST['content'],$_POST['tag'],$affcomment,$draft, $private, $author_id, $date,$_POST['id_news']));
			delete_file('app/cache/tmp/article/'.$_POST['id_news'].'.html');
			erased_dir('app/cache/tmp/index');
			erased_dir('app/cache/tmp/feed');
			push_pshb();
		}
	}

	if (isset($_GET['delete_news']) && !empty($_SESSION['token']) && $_SESSION['token']==$_GET['token'] && DEMO !== true) {
		sql('DELETE FROM '.PREFIX.'news WHERE id=?', array($_GET['delete_news']));
		sql('DELETE FROM '.PREFIX.'comment WHERE idnews=?', array($_GET['delete_news']));
		delete_file('app/cache/tmp/article/'.$_GET['delete_news'].'.html');
		erased_dir('app/cache/tmp/index');
		erased_dir('app/cache/tmp/feed');
		push_pshb();
	}

//listage des news
	//On compte le nombre de news, et s'il y en a plus de 10, ont fait une autre page.
	$nb_messages_pages = NB_MESSAGES_PAGE*10;
	$nbPages = ceil($data['count(id)'] / $nb_messages_pages);
	$page = 0;
	if(isset($_GET['page']) && (intval($_GET['page']) <= $nbPages)) {
		$page = intval($_GET['page']) - 1;
	}
	else {$_GET['page'] = 1;}
	$where = '';
	if($_SESSION['privilege'] == "1") {
		$where .= 'n.author_id = a.id_author';
		if(isset($_GET['author'])) {
			$where .=' AND n.author_id = "'.$_GET['author'].'"';
		}
	}
	else {
		$where .= 'n.author_id ="'.$_SESSION['author_id'].'" AND n.author_id = a.id_author';
	}
	if(isset($_GET['draft'])) {
		if($_GET['draft'] == 1) {
			$where .=' AND n.draft = "1"';
			$value_draft = 0;
		}
		elseif($_GET['draft'] == 0) {
			$where .=' AND n.draft = "0"';
			$value_draft = 2;
		}
		else {
			$value_draft = 1;
		}
	}
	else {
		$value_draft = 1;
	}
	if(isset($_GET['date'])) {
		if($_GET['date'] == 1) {
			$date_sql ='ASC';
			$value_date = 0;
		}
		elseif($_GET['date'] == 0) {
			$date_sql ='DESC';
			$value_date = 1;
		}
		else {
			$date_sql ='DESC';
			$value_date = 1;
		}
	}
	else {
		$date_sql ='DESC';
		$value_date = 1;
	}
	$something = $bdd->query('SELECT n.id, n.title, n.timestamp, n.draft, n.author_id, a.name FROM '.PREFIX.'news n, '.PREFIX.'author a WHERE '.$where.' ORDER BY n.timestamp '.$date_sql.' LIMIT '.$nb_messages_pages * $page.', '.$nb_messages_pages);
	debug('query');
	echo '<table>'.PHP_EOL;
	echo "\t\t".'<tr>'.PHP_EOL;
	echo "\t\t"."\t\t".'<th>'.translate('modify').'</th>'.PHP_EOL;
	echo "\t\t"."\t\t".'<th>'.translate('remove').'</th>'.PHP_EOL;
	echo "\t\t"."\t\t".'<th>'.translate('title').'</th>'.PHP_EOL;
	echo "\t\t"."\t\t".'<th><a href="?date='.$value_date.'">'.translate('date').'</a></th>'.PHP_EOL;
	echo "\t\t"."\t\t".'<th><a href="?draft='.$value_draft.'">'.translate('draft').'</a></th>'.PHP_EOL;
	echo "\t\t"."\t\t".'<th>'.translate('author').'</th>'.PHP_EOL;
	echo "\t\t".'</tr>'.PHP_EOL;
	while($data = $something->fetch()) {
		echo "\t\t".'<tr>'.PHP_EOL;
		echo "\t\t"."\t\t".'<td><a href="admin.php?modify_news='.$data['id'].'&action=addnews"><img src="app/design/modify.png" alt="'.translate('modify').'"></a></td>'.PHP_EOL;
		echo "\t\t"."\t\t".'<td><a href="admin.php?delete_news='.$data['id'].'&token='.$_SESSION['token'].'"><img src="app/design/remove.png" alt="'.translate('remove').'"></a></td>'.PHP_EOL;
		echo "\t\t"."\t\t".'<td><a href="'.url_format($data['id'],FORMAT_URL_POST).'">'.$data['title'].'</a></td>'.PHP_EOL;
		echo "\t\t".'<td>'.format_date(FORMAT_DATE, $data['timestamp']).'</td>'.PHP_EOL;
			echo "\t\t"."\t\t".'<td>'.PHP_EOL;
		if($data['draft'] == '1') { echo '<img src="app/design/validation.png" alt="'.translate('draft').'"/>'; }
		echo "\t\t".'<td><a href="?author='.$data['author_id'].'">'.$data['name'].'</a></td>'.PHP_EOL;
		echo "\t\t"."\t\t".'</td>'.PHP_EOL;
		echo "\t\t".'</tr>'.PHP_EOL;
	} 

		echo '</table>';
	$pagination = pagination($_GET['page'], $nbPages);
	if(tpl_include('app/design/'.DESIGN.'/tpl/pagination.php')) {} else { echo '<div id="pagination">'.$pagination.'</div>'; }
	}

}
else {
	$form = New form(array('method'=>'post', 'action'=>'?check=check'));
	$form->label('login', translate('login'));
	$form->input(array('type'=>'text', 'name'=>'login'));
	$form->label('password', translate('password'));
	$form->input(array('type'=>'password', 'name'=>'password'));
	$checked_cookie = (isset($_COOKIE['auth'])) ? true : false;
	$form->label('cookie',translate('cookie'));
	$form->checkbox(array('type'=>'checkbox', 'name'=>'cookie', 'value'=>'cookie', 'id'=>'cookie'), $checked_cookie);
	$form->input(array('type'=>'submit', 'name'=>'submit'));
	$form->endform();
}
include 'app/design/'.DESIGN.'/tpl/footer.php';
debug('bottom');
if(DEBUG == true) {var_dump(get_defined_vars());}
?>