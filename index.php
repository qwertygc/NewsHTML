<?php
if(extension_loaded('zlib')){ob_start('ob_gzhandler');}
session_start();
include 'app/inc/system.php';
debug('top');
if (isset($_POST['cookie'])) { //On crée les cookies
	$cookie = array();
	$cookie['pseudo'] = (isset($_POST['pseudo'])) ?  strip_tags($_POST['pseudo']) : null;
	$cookie['email'] = (isset($_POST['email'])) ?  strip_tags($_POST['email']) : null;
	$cookie['website'] = (isset($_POST['website'])) ?  strip_tags($_POST['website']) : null;
	$cookie['notif'] = (isset($_POST['notif'])) ?  strip_tags($_POST['notif']) : null;
	setcookie('comment', serialize($cookie), time() + 365*24*3600, null, null, false, true);
}
if(isset($_GET['id'])) {
	$_GET['id'] = intval($_GET['id']);
	### caching system####
	$cache = 'app/cache/tmp/article/'.$_GET['id'].'.html';
	if(check_cache($cache)) {readfile($cache);}
	else {
		ob_start();
		#### show article ####
	//Selection de la news choisi
		$something = $bdd->prepare('SELECT n.*, a.id_author, a.name FROM '.PREFIX.'news n, '.PREFIX.'author a WHERE n.author_id = a.id_author AND n.id=?');
		$something->execute(array(intval($_GET['id'])));
		debug('query');
		$data = $something->fetch();
		//Si on a pas coché l'affichage des comment, on le met sur false
		$comment = ($data['affcomment'] == '1') ? true : false;
		file_put_contents('app/cache/tmp/article/'.$_GET['id'].'.comment', $data['affcomment']);
		$title = $data['title'];
		$link = $data['link'];
		$author = '<a href="'.url_format($data['author_id'],FORMAT_URL_AUTHOR).'" rel="author">'.$data['name'].'</a>';
		$date = format_date(FORMAT_DATE, $data['timestamp']);
		$datetime= date('Y-m-d H:i:s', $data['timestamp']);
		$content = parse($data['content'], $data['id']);
		$readingtime = translate('readingtime').' '.readingtime(strip_tags($content));
		$id = intval($_GET['id']);
		$article_publish = (time()> $data['timestamp'] AND $data['draft']=='0' and isset($data['id'])) ? true : false;
		$subtitle = ($article_publish == true) ? $title : '';
		include 'app/design/'.DESIGN.'/tpl/header.php';
		if ($comment == true) {
			$infos_com =$bdd->prepare('SELECT count(*) AS nbr FROM '.PREFIX.'comment WHERE idnews=? AND moderation=1');
			$infos_com->execute(array(intval($data['id'])));
			$data_nb = $infos_com->fetch();
			debug('query');
			$nbcomment = ($data_nb['nbr'] <= 1) ?  $data_nb['nbr']." ".translate('comment0') : $data_nb['nbr']." ".translate('comment');
		}
		else {
			$nbcomment = '';
		}
		$tags = '';
		// $tags .= translate('tag').' : ';
		$tag = explode(TAG_SEPARATOR, $data['tag']);
		if(isset($data['tag'])) {
			$count = count($tag);
			for ($numero = 0; $numero < $count; ++$numero) {
				$tag_separator = ($numero == $count-1) ? '' : TAG_SEPARATOR;
				$tags .= '<a href="'.url_format($tag[$numero], FORMAT_URL_TAG).'">'.$tag[$numero].'</a>'.$tag_separator;
			}
		}
		if($article_publish == true && (($data['private'] !='' AND isset($_SESSION['article'][$_GET['id']]) AND $_SESSION['article'][$_GET['id']] == true) || $data['private'] =='') OR isset($_SESSION['login'])) {
			include 'app/design/'.DESIGN.'/tpl/article.php';
			//On selectionne les comment pour les envoyer.
			$something = $bdd->prepare('SELECT * FROM '.PREFIX.'comment WHERE idnews=:id AND moderation=1 ORDER BY id ASC');
			$something->execute(array('id'=>$id));
			debug('query');
			$something->setFetchMode(PDO::FETCH_BOTH);
			while($data = $something->fetch()){
					$email = htmlspecialchars_decode($data['email']);
					$website = htmlspecialchars_decode($data['website']);
					$message = htmlspecialchars_decode(parse($data['message'], $data['id']));
					$id = $data['id'];
					$date = format_date(FORMAT_DATE, $data['timestamp']);
					$datetime = date('Y-m-d H:i:s', $data['timestamp']);
					$gravatar = '<img src="app/cache/get.php?g='. md5($email) .'" alt="Gravatar" title="Gravatar" />';
					$pseudo = (!empty($website)) ? '<a href="' .$website. '">'. $data['pseudo'].'</a>' : $data['pseudo'];
					include 'app/design/'.DESIGN.'/tpl/comment.php';
			}
		}
		elseif($data['private'] !='' AND $_SESSION['article'][$data['id']] == false) {
			echo private_form($data['id']);
			$comment = false;
			delete_file('app/cache/tmp/article/'.$data['id'].'.html');
		}
		else {
			redirect('index.php');
		}
		### caching system####
		$page = ob_get_contents();
		ob_end_clean();
		file_put_contents($cache, $page);
		chmod($cache, 0755);
		echo $page;
	}
	$comment = (isset($comment)) ? $comment : ((file_get_contents('app/cache/tmp/article/'.$_GET['id'].'.comment') == '1') ? true : false);
	if($comment == true) {
		tpl_include('app/design/'.DESIGN.'/tpl/comment_msg.php');
		$cookie = (isset($_COOKIE['comment'])) ? unserialize($_COOKIE['comment']) : null;
		## Form of comment ##
		$form = New form(array('method'=>'post'));
		$form->label('pseudo', translate('pseudo'));
		$cookie_pseudo = (isset($cookie['pseudo'])) ? $cookie['pseudo'] : '';
		$form->input(array('type'=>'text', 'name'=>'pseudo','value'=>$cookie_pseudo, 'placeholder'=>translate('pseudo')));
		$form->label('email', translate('email'));
		$cookie_mail = (isset($cookie['email'])) ? $cookie['email'] : '';
		$form->input(array('type'=>'email', 'name'=>'email','value'=>$cookie_mail, 'placeholder'=>translate('email')));
		$form->label('website', translate('website'));
		$cookie_site = (isset($cookie['website'])) ? $cookie['website'] : '';
		$form->input(array('type'=>'url', 'name'=>'website','value'=>$cookie_site, 'placeholder'=>translate('website')));
		$form->label('message', translate('message'));
		$form->textarea(array('name'=>'message', 'rows'=>'5', 'cols'=>'25', 'placeholder'=>translate('message')), '');
		$checked_cookie = (isset($cookie['pseudo'])) ? true : false;
		$form->label('cookie',translate('cookie'));
		$form->checkbox(array('type'=>'checkbox', 'name'=>'cookie', 'value'=>'cookie', 'id'=>'cookie'), $checked_cookie);
		$checked_notif = (isset($cookie['notif'])) ? true : false;
		$form->label('notif', translate('notifmail'));
		$form->checkbox(array('type'=>'checkbox', 'name'=>'notif', 'value'=>'notif', 'id'=>'notif'), $checked_notif);
		$form->input(array('type'=>'hidden', 'name'=>'idcomment','value'=>$_GET['id']));
		if(CAPTCHA != '') {
			$form->label('captcha', translate('typecaptcha'));
			$value_captcha = (isset($cookie['pseudo'])) ? CAPTCHA : '';
			$form->input(array('type'=>'text', 'name'=>'captcha', 'id'=>'captcha', 'value'=>$value_captcha));
		}
		else {
			$_POST['captcha'] = '';
		}
		$form->input(array('type'=>'submit'));
		$form->endform();
		// On rajoute dans la base de données le comment, et on recherche les personnes inscrite pour recevoir un email, pour enfin leur l'envoyer.
		if (isset($_POST['message']) AND isset($_POST['pseudo']) && !empty($_POST['message']) && !ctype_space($_POST['message']) && !ctype_space($_POST['pseudo']) && $_POST['captcha']==CAPTCHA && DEMO !== true) {
			$moderation = (MODERATION == true) ? '0':'1';
			$pleaseexpect = (MODERATION == true) ? 'The comment must be moderate and can be not available. Please expect during the validation.':'';
			$notif = (isset($_POST['notif'])) ? '1':'0';
			sql("INSERT INTO ".PREFIX."comment(pseudo, email, website, notif, message,idnews, moderation, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?, ?)", array(htmlspecialchars($_POST['pseudo']),htmlspecialchars($_POST['email']),htmlspecialchars($_POST['website']), $notif, htmlspecialchars(strip_tags($_POST['message'])), $_POST['idcomment'], $moderation, time()));
			push_pshb();
			$lastidcomment = $bdd->lastInsertId();//On récupère l'id du commentaire pour l'ancre du mail
			$something = $bdd->prepare('SELECT DISTINCT email, title, idnews FROM `'.PREFIX.'news` LEFT JOIN `'.PREFIX.'comment` ON `'.PREFIX.'comment`.`idnews` = `'.PREFIX.'news`.`id` WHERE idnews=? AND notif="1"');
			$something->execute(array($_GET['id']));
			debug('query');
			while($data = $something->fetch()) {
				$msg = "A new comment by *".htmlspecialchars($_POST['pseudo'])."* has been posted on *".$data['title']."* from *".TITLE."*. ".$pleaseexpect.PHP_EOL."
				You can see it by following this link: ".ROOT.url_format($data['idnews'],FORMAT_URL_POST)."#".$lastidcomment.PHP_EOL."
				To unsubscribe from the comments on that post, you can follow this link: ".ROOT.url_format($data['idnews'],FORMAT_URL_POST)."&unsub=1&comment=".$data['idnews']."&email=".md5($data['email']).PHP_EOL."
				To unsubscribe from the comments on all posts, you can follow this link: ".ROOT.url_format($data['idnews'],FORMAT_URL_POST)."&unsub=1&mail=".md5($data['email']).PHP_EOL."
				Also, do not reply to this email, since it is an automatic generated email.".PHP_EOL."
				Regards.";
				if($data['email'] != $_POST['email']) {
					send_mail($data['email'], EMAIL, 'New comment on '.$data['title'].'from '.TITLE, $msg);
				}
			}
			delete_file('app/cache/tmp/article/'.$_GET['id'].'.html');
			erased_dir('app/cache/tmp/index');
			redirect(url_format($_GET['id'],FORMAT_URL_POST));
		}
		//Désinscription de l'email de suivi
		if(isset($_GET['unsub']) AND $_GET['unsub'] == '1') {
			$something = $bdd->prepare("SELECT * FROM ".PREFIX."comment WHERE id=?");
			$something->execute(array($_GET['comment']));
			debug('query');
			$something->setFetchMode(PDO::FETCH_BOTH);
			$data = $something->fetch();
			if($_GET['email'] == md5($data['email'])) {
				if(isset($_GET['comment'])) { //On supprime juste la suivi du post
					sql("UPDATE ".PREFIX."comment SET notif=0 WHERE idnews=:comment AND email=:email", array('comment'=>$_GET['comment'], ':email'=>$data['email']));
				}
				else { //On supprime tous les suivis
					sql("UPDATE ".PREFIX."comment SET notif=0 WHERE email=:email", array('email'=>$data['email']));
				}
			}
		}
	}
	include 'app/design/'.DESIGN.'/tpl/footer.php';
}

else {
	#### caching system####
	if(isset($_GET['page'])) $_GET['page'] = intval($_GET['page']); else $_GET['page'] = 1 ;
	$cache = 'app/cache/tmp/index/'.$_GET['page'].'.html';
	if(check_cache($cache)) {readfile($cache);}
	else {
		ob_start();
			include 'app/design/'.DESIGN.'/tpl/header.php';
		####show articles####
		//On compte le nombre de news, et s'il y en a plus de 10, ont fait une autre page.
		$something = $bdd->query('SELECT COUNT(*) AS nb_messages FROM '.PREFIX.'news WHERE timestamp < '.time().' AND draft="0" AND private = ""');
		$data = $something->fetch();
		debug('query');
		$nbPages = ceil($data['nb_messages'] / NB_MESSAGES_PAGE);
		$page = 0;
		if(isset($_GET['page']) && (intval($_GET['page']) <= $nbPages)) {
			$page = intval($_GET['page']) - 1;
		}
	 
		$something = $bdd->query('SELECT * FROM '.PREFIX.'news n, '.PREFIX.'author a WHERE n.timestamp < '.time().' AND n.draft="0"  AND private = "" AND n.author_id = a.id_author ORDER BY n.timestamp DESC LIMIT '.NB_MESSAGES_PAGE * $page.', '.NB_MESSAGES_PAGE);
		$something->setFetchMode(PDO::FETCH_BOTH);
		debug('query');
		while($data = $something->fetch()) {
			if($comment == true) {
				$infos_com =$bdd->prepare('SELECT count(*) AS nbr FROM '.PREFIX.'comment WHERE idnews=? AND moderation=1');
				debug('query');
				$infos_com->execute(array(intval($data['id'])));
				$data_nb = $infos_com->fetch();
				$nbcomment = ($data_nb['nbr'] <= 1) ?  $data_nb['nbr']." ".translate('comment0') : $data_nb['nbr']." ".translate('comment');
				}
			else {
				$nbcomment = '';
			}
			$title = $data['title'];
			$link = $data['link'];
			$id = $data['id'];
			$author = '<a href="'.url_format($data['author_id'], FORMAT_URL_AUTHOR).'" rel="author">'.$data['name'].'</a>';
			$date = format_date(FORMAT_DATE, $data['timestamp']);
			$datetime = date('Y-m-d H:i:s', $data['timestamp']);
			$content = (TEXT_RESUME == true) ? texte_resume(parse($data['content'], $data['id']), TEXT_RESUME_NB) : parse($data['content'], $data['id']);
			;
			$readingtime = translate('readingtime').' '.readingtime(strip_tags($content));
			$tags = '';
			// $tags .= translate('tag').' : ';
			$tag = explode(TAG_SEPARATOR, $data['tag']);
			$count = count($tag);
			for ($numero = 0; $numero < $count; ++$numero) {
				$tag_separator = ($numero == $count-1) ? '' : TAG_SEPARATOR;
				$tags .= '<a href="'.url_format($tag[$numero], FORMAT_URL_TAG).'">'.$tag[$numero].'</a>'.$tag_separator;
			}	
			if(time()> $data['timestamp']) {
				include 'app/design/'.DESIGN.'/tpl/article.php';
			}
		}
		$pagination = pagination($_GET['page'], $nbPages, FORMAT_URL_PAGINATION);
		if(file_exists('app/design/'.DESIGN.'/tpl/pagination.php')) {tpl_include('app/design/'.DESIGN.'/tpl/pagination.php');} else { echo '<div id="pagination">'.$pagination.'</div>'; }
		include 'app/design/'.DESIGN.'/tpl/footer.php';
		### caching system####
		$page = ob_get_contents();
		ob_end_clean();
		file_put_contents($cache, $page);
		chmod($cache, 0755);
		echo $page;
	}
}
########## PRIVATE NEWS ###############
if(isset($_POST['password']) AND isset($_POST['id_news'])) {
	$something = $bdd->prepare("SELECT * FROM ".PREFIX."news WHERE private =? AND id=?");
	$something->execute(array($_POST['password'], $_POST['id_news']));
	$data = $something->fetch();
	if($data == true) {
		$_SESSION['article'][$_POST['id_news']] = true;
		delete_file('app/cache/tmp/article/'.$_POST['id_news'].'.html');
		redirect(url_format($_POST['id_news'],FORMAT_URL_POST));
	}
}
debug('bottom');
if(DEBUG == true) {var_dump(get_defined_vars());}
?>