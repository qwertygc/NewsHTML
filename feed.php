<?php
if(extension_loaded('zlib')){ob_start('ob_gzhandler');}
// header('Content-type: application/xml');
include 'app/inc/system.php';
debug('top');
header('Content-Type: application/atom+xml;charset=utf-8');
### caching system####
	if(!is_dir('app/cache/tmp')) { mkdir('app/cache/tmp');}
	
	if(isset($_GET['links'])) { $cache = 'links.xml';}
	elseif((isset($_GET['tag']) && $_GET['tag'] != NULL) && !isset($_GET['author'])) {$cache = ''.$_GET['tag'].'.xml';}
	elseif((isset($_GET['tag']) && $_GET['tag'] != NULL) && (isset($_GET['author']) && $_GET['author'] != NULL)) {$cache = 'author-'.$_GET['author'].'-'.$_GET['tag'].'.xml';}
	elseif(!isset($_GET['tag']) && isset($_GET['author']) && $_GET['author'] != NULL) {$cache = 'author-'.$_GET['author'].'.xml';}
	elseif(isset($_GET['comment']) && $_GET['comment'] !=NULL) {$cache = 'comment-'.$_GET['comment'].'.xml';}
	else {$cache = 'atom.xml';}
	
	if(file_exists($cache) && filemtime($cache) >time()-3600  && DEBUG == false) {readfile('app/cache/tmp/feed/'.$cache);}
	else {
		ob_start();
	if(isset($_GET['tag']) && $_GET['tag'] != NULL) {
		$query = htmlspecialchars($_GET['tag']); 
		$motrecherche=explode(TAG_SEPARATOR,$query); 
	//on créée une variable
		foreach($motrecherche as $query) {
			@$where.="tag LIKE :query OR ";
		}
		$where=substr($where, 0,-4);
		if(isset($_GET['author']) && $_GET['author'] != NULL) {
			$sql = $bdd->prepare('SELECT n.*, a.id_author, a.name FROM '.PREFIX.'news n, '.PREFIX.'author a WHERE '.$where.' AND author_id=:author AND n.author_id = a.id_author AND draft=0 AND :time >= timestamp AND private ="" ORDER BY timestamp DESC');
			$sql->execute(array('query'=>'%'.$query.'%', 'author'=>intval($_GET['author']), 'time'=>time()));
		}
		else {
			$sql = $bdd->prepare('SELECT n.*, a.id_author, a.name FROM '.PREFIX.'news n, '.PREFIX.'author a WHERE '.$where.' AND draft=0 AND n.author_id = a.id_author AND :time >= timestamp AND private ="" ORDER BY timestamp DESC');
			$sql->execute(array('query'=>'%'.$query.'%', 'time'=>time()));
		}
		debug('query');
	}
	elseif(isset($_GET['author']) && $_GET['author'] != NULL) {
		$sql = $bdd->prepare('SELECT n.*, a.id_author, a.name FROM '.PREFIX.'news n, '.PREFIX.'author a WHERE author_id=:id AND draft=0 AND n.author_id = a.id_author AND :time >= timestamp AND private ="" ORDER BY timestamp DESC');
		$sql->execute(array('id'=>intval($_GET['author']), 'time'=>time()));
		debug('query');
	}
	elseif(isset($_GET['links'])) {
		$sql = $bdd->prepare('SELECT n.*, a.id_author, a.name FROM '.PREFIX.'news n, '.PREFIX.'author a WHERE draft=0 AND n.author_id = a.id_author AND :time >= timestamp AND private ="" AND link !="" ORDER BY timestamp DESC');
		$sql->execute(array('time'=>time()));
	}
	elseif(isset($_GET['comment'])) {
		$sql = $bdd->prepare('SELECT c.id, c.pseudo, c.message, c.idnews, c.timestamp, c.moderation, n.title, n.id FROM '.PREFIX.'comment c, '.PREFIX.'news n WHERE c.idnews =:comment AND c.moderation =1 AND c.idnews = n.id AND private ="" ORDER BY c.id DESC');
		$sql->execute(array('comment'=>$_GET['comment']));
	}
	else {
		$sql = $bdd->prepare('SELECT n.*,  a.id_author, a.name FROM '.PREFIX.'news n, '.PREFIX.'author a WHERE draft=0 AND n.author_id = a.id_author AND :time >= timestamp AND private ="" ORDER BY timestamp DESC LIMIT 0,'.NB_MESSAGES_PAGE.'');
		$sql->execute(array('time'=>time()));
		debug('query');
	}
	if(isset($_GET['links'])) {
		$atom = New Atom();
		$atom->title(TITLE);
		$atom->subtitle(DESCRIPTION);
		$atom->link(ROOT.'/');
		$atom->id(parse_url(ROOT)['host']);
		$sql->setFetchMode(PDO::FETCH_BOTH);
		while($data = $sql->fetch()) {
			$atom->startentry();
			$atom->title($data['title']);
			$atom->author($data['name']);
			$atom->link($data['link']);
			$atom->id(''.parse_url(ROOT)['host'].','.date('Y-m-d', $data['timestamp']).':blog');
			$atom->published($data['timestamp']);
			// $atom->updated($data['timestamp']);
			// $atom->summary(parse($data['message'], $data['id']));
			$atom->content(parse($data['content'], $data['id']).'(<a href="'.url_format($data['id'],FORMAT_URL_POST).'">permalink</a>)');
			$tags = explode(TAG_SEPARATOR,$data['tag']);
			foreach($tags as $tag) {
				echo '<category scheme="'.ROOT.'" term="'.$tag.'" />';
			}
			$atom->endentry();
		}
	}
	elseif(isset($_GET['comment'])) {
		$atom = New Atom();
		$atom->title(TITLE);
		$atom->subtitle(DESCRIPTION);
		$atom->link(url_format($_GET['comment'], FORMAT_URL_POST));
		$atom->id(parse_url(ROOT)['host']);
		$sql->setFetchMode(PDO::FETCH_BOTH);
		while($data = $sql->fetch()) {
			$atom->startentry();
			$atom->title($data['title'].' - '.$data['pseudo']);
			$atom->author($data['pseudo']);
			$atom->link(url_format($data['idnews'],FORMAT_URL_POST).'#'.$data['id']);
			$atom->id(''.parse_url(ROOT)['host'].','.date('Y-m-d', $data['timestamp']).':comment');
			$atom->published($data['timestamp']);
			// $atom->updated($data['timestamp']);
			// $atom->summary(parse($data['message'], $data['id']));
			$atom->content(parse($data['message'], $data['id']));
			$atom->endentry();
		}
	}
	else {
		$atom = New Atom();
		$atom->title(TITLE);
		$atom->subtitle(DESCRIPTION);
		$atom->link(ROOT.'/');
		$atom->id(parse_url(ROOT)['host']);
		$sql->setFetchMode(PDO::FETCH_BOTH);
		while($data = $sql->fetch()) {
			$atom->startentry();
			$atom->title($data['title']);
			$atom->author($data['name']);
			$atom->link(url_format($data['id'],FORMAT_URL_POST));
			$atom->id(''.parse_url(ROOT)['host'].','.date('Y-m-d', $data['timestamp']).':blog');
			$atom->published($data['timestamp']);
			// $atom->updated($data['timestamp']);
			// $atom->summary(parse($data['message'], $data['id']));
			$atom->content(parse($data['content'], $data['id']));
			$atom->endentry();
		}
	}
	### caching system####
	$page = ob_get_contents();
	ob_end_clean();
	file_put_contents('app/cache/tmp/feed/'.$cache, $page);
	chmod('app/cache/tmp/feed/'.$cache, 0755);
	echo $page;
}
debug('bottom');
if(DEBUG == true) {var_dump(get_defined_vars());}
?>